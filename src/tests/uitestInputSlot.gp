to testInputSlot {
  page = (newPage 400 300 (color 100 100 80))
  open page

  is = (newInputSlot 'Hello World' 'line')
  setPosition (morph is) 50 20
  addPart page is

  is = (newInputSlot '42' 'numerical')
  setPosition (morph is) 50 80
  addPart page is

  is = (newInputSlot (join 'Hello' (string 13) 'World') 'editable')
  setPosition (morph is) 50 140
  addPart page is

  startStepping page
}

testInputSlot
