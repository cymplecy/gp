call (function {
  a = 1
  b = 2
  c = 3
  d = -1
  e = -2
  f = -3

  assert (a & b) 0 'a & b'
  assert (a | b) 3 'a | b'

  assert (a & c) 1 'a & c'
  assert (a | c) 3 'a | c'

  assert (d & e) -2 'd & e'
  assert (d | e) -1 'd | e'

  assert (d & f) -3 'd & f'
  assert (e | f) -1 'e | f'

  assert (a ^ b) 3  'a ^ b'
  assert (b ^ b) 0 'b ^ b'
  assert (c ^ a) 2 'c ^ a'

  assert (d ^ e) 1 'd ^ e'

  assert (a << 2) 4 'a << 2'
  assert (b << 2) 8 'b << 2'

  assert ((a << 2) >> 1) 2 '(a << 2) >> 1'
  assert ((b << 2) >> 3) 1 '(b << 2) >> 3'

  assert ((b << 2) >> 4) 0 '(b << 2) >> 4'

  assert (d << 2) -4 'd << 2'
  assert (e << 2) -8 'e << 2'

  assert ((d << 2) >> 1) -2 '(d << 2) >> 1'
  assert ((d << 2) >>> 1) -2 '(d << 2) >>> 1'

  assert ((e << 2) >>> 2) 1073741822 '(e << 2) >>> 2'
  assert ((e << 2) >>> 3) 536870911 '(e << 2) >>> 3'

//  assert (| 0 0 13) 13 'var args or'
//  assert (& 15 7 2) 2 'var args and'

})


