// test and demonstrate grips

to gripDemo {
  page = (newPage 400 300)
  open page

  b = (newGrip 'horizontal')
  setPosition (morph b) 50 20
  addPart (morph page) (morph b)

  b = (newGrip 'vertical')
  setPosition (morph b) 20 50
  addPart (morph page) (morph b)

  b = (newGrip 'free')
  setPosition (morph b) 50 50
  addPart (morph page) (morph b)

  startStepping page
}

gripDemo
