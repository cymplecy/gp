call (function {
cmdList = (at (parse '{
  (setShared ''X'' (a * 42))
  tmp = 3
  (setShared ''Y'' (tmp * tmp))
  }') 1)

argNames = (array 'a' 'b')
fields = (array 'd' 'e')
moduleVars = (array 'X' 'Y')

locals = (toList (keys (collectLocals cmdList)))
removeAll locals argNames
removeAll locals fields
removeAll locals moduleVars

assert locals (list 'tmp') 'collectLocals'
})
