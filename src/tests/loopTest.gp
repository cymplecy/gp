to loopTest {
  timer = (newTimer)
  count = 0
  repeat 100000000 { count += 1 }
  log (msecs timer) 'msecs'
}

loopTest
