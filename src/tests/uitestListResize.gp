// test resizing a scrolled list box

to demo {
  page = (newPage 800 800)
  open page

  clr = (color 220 220 255)

  cls = (listBox (classes) 'className' 'nop' clr)
  sf = (scrollFrame cls clr)
  setPosition (morph sf) 50 50
  setExtent (morph sf) 100 300
  setGrabRule (morph sf) 'handle'
  addPart (morph page) (morph sf)
  resizeHandle sf

  startStepping page
}

demo
