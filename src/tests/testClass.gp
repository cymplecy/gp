call (function {
assertNotEqual (class 'Nil') nil '(class Nil)'
assertNotEqual (class 'Boolean') nil '(class Boolean)'
assertNotEqual (class 'Integer') nil '(class Integer)'
assertNotEqual (class 'String') nil '(class String)'
assertNotEqual (class 'Array') nil '(class Array)'
assertNotEqual (class 'Class') nil '(class Class)'
assertNotEqual (class 'Function') nil '(class Function)'
assertNotEqual (class 'Command') nil '(class Command)'
assertNotEqual (class 'Reporter') nil '(class Reporter)'
assert (classOf 'hello') (class 'String'), '(classOf hello)'

c = (count (classes))
defineClass Point3D x y z
assert (c + 1) (count (classes)) '(c + 1)'

p1 = (new 'Point3D' 10 20 30)
assert (classOf p1) (class 'Point3D'), '(classOf p1)'

assert (getField p1 'x') 10 '(getField p1 x)'
assert (setField p1 'y' 1) nil '(setField p1 y 1)'
assert (getField p1 'y') 1 '(getField p1 y)'

p2 = (newIndexable 'Point3D' 2)
assert (classOf p2) (class 'Point3D'), '(classOf p2)'
assert (getField p2 'z') nil '(getField p2 z)'
assert (getField p2 5) nil '(getField p2 5)'
})
