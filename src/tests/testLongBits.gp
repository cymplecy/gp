call (function {
  l = (| ((toLargeInteger 128) << 24) (64 << 16) (32 << 8) (16 << 0))
  assert (toStringBase16 l) '80402010' 'long'
})
