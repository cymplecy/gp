// Test method cache invalidation

defineClass CacheTest1
defineClass CacheTest2
method at CacheTest1 { return 'CacheTest1' }
method at CacheTest2 { return 'CacheTest2' }

to testCache obj { return (at obj 1) }

assert (testCache (new 'CacheTest1')) 'CacheTest1' 'CacheTest: 1'
assert (testCache (new 'CacheTest2')) 'CacheTest2' 'CacheTest: 2'
assert (testCache (array 1)) 1 'CacheTest: 3'

assert (testCache (new 'CacheTest1')) 'CacheTest1' 'CacheTest: 4'
assert (testCache (new 'CacheTest2')) 'CacheTest2' 'CacheTest: 5'
assert (testCache (array 1)) 1 'CacheTest: 6'
