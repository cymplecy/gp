// test and demonstrate the pageResized event

defineClass PageFiller morph

method init PageFiller {morph = (newMorph this)}

method redraw PageFiller {
  bm = (newBitmap (width (bounds morph)) (height (bounds morph)) (color 100 100 0))
  setCostume morph bm
}

method pageResized PageFiller x y aPage {
  setPosition morph ((left (bounds (morph aPage))) + 10) ((top (bounds (morph aPage))) + 10)
  setExtent morph (x - 20) (y - 20)
}

to demo {
  page = (newPage 500 300)
  open page

  pf = (new 'PageFiller')
  init pf
  addPart page pf
  pageResized pf 500 300 page

  startStepping page
}

demo
