to test {
  openWindow 300 300
  centerX = 150
  centerY = 150
  pen = (newPen)
  setColor pen (color 0 0 200)
  setX pen 140
  setY pen 140
  repeat 10 {
    down pen
    for step 10 {
      dest = (scaleAndRotateAround (x pen) (y pen) 36 centerX centerY)
      goto pen (first dest) (last dest) true 1
    }
    s += 0.5
    setX pen ((x pen) - 10)
    setY pen ((y pen) - 10)
  }
  setColor pen (color 200 0 0)
  lw = 40
  for step 40 {
    dest = (scaleAndRotateAround (x pen) (y pen) 36 centerX centerY 0.9)
    setLineWidth pen lw
    goto pen (first dest) (last dest)
    lw = (lw * 0.9)
  }
  flipBuffer
  sleep 2000
}

test
