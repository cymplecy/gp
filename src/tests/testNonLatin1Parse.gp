call (function {
str = 'to Mönig {
  return ''Mönig''
}'

cmds = (parse str)
assert (count cmds) 1 'parse count'
assert (classOf (at cmds 1)) (class 'Reporter') 'parse 1'

eval (at cmds 1)

assert (Mönig) 'Mönig'
})
