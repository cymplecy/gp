call (function {
  moduleDef = '
module
  moduleVariables X Y
  moduleExports Foo foo

  defineClass Foo a b
  method doFoo Foo x y {return (X + y)}
  method doBar Foo x y {return (x + Y)}

  to foo x y {return x - y}

  to initializeModule {
    setShared ''X'' 42
    setShared ''Y'' 666
  }
'
  m = (initialize (new 'Module'))
  loadModuleFromString m moduleDef
  callInitializer m

  assert (classOf m) (class 'Module') 'class of module'
  assert (classOf (getField m 'classes')) (class 'Array') 'type of classes'
  assert (classOf (getField m 'variables')) (class 'Array') 'type of variables'
  assert (classOf (getField m 'variables')) (class 'Array') 'type of variableNames'
  assert (classOf (getField m 'functions')) (class 'Array') 'type of functions'

  c = (at (getField m 'classes') 1)
  assert (className c) 'Foo' 'class name'
  assert (module c) m 'module of class'

  assert (count (methods c)) 2 'methods count'
  assert (module (methodNamed c 'doFoo')) m 'module of method 1'
  assert (module (methodNamed c 'doBar')) m 'module of method 2'

  f = (at (getField m 'functions') 1)
  assert (module f) m 'module of function'

  c = (getField m 'variableNames')
  assert (at c 1) 'X' 'var X'
  assert (at c 2) 'Y' 'var Y'

  c = (getField m 'variables')
  assert (at c 1) 42
  assert (at c 2) 666
})
