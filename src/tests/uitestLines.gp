to drawDial pen x y antialiasing {
  setDirection pen 0
  for i 36 {
    up pen
    setX pen x
    setY pen y
    move pen 25 antialiasing
    down pen
    move pen 75 antialiasing
    turn pen 10
  }
}

to test {
  openWindow
  bm = (newBitmap 500 500)
  pen = (newPen bm)
  drawDial pen 110 110 false
  drawDial pen 320 110 true
  setLineWidth pen 4
  drawDial pen 110 320 false
  drawDial pen 320 320 true
  clearBuffer
  drawBitmap nil bm 0 0
  flipBuffer
  sleep 2000
}

test
