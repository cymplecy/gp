(call (function {
  b = (readFile 'tests/gp.png' true)

  bitmap = (readFrom (new 'PNGReader') b)

  assert (width bitmap) 211 'width'
  assert (height bitmap) 207 'height'
  assert (byteCount (getField bitmap 'pixelData')) (* (width bitmap) (height bitmap) 4)

  openWindow
  clearBuffer
  drawBitmap nil bitmap 0 0
  flipBuffer

  sleep 2000
}))
