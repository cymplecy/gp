// test drawing tabs

to demo {
  page = (newPage)
  open page

  tab = (newBox nil (color 0 0 255) 0 0 false)
  setExtent (morph tab) 300 150
  setGrabRule (morph tab) 'handle'
  bm = (newBitmap (width (bounds (morph tab))) (height (bounds (morph tab))))
  pen = (newPen bm)
  setColor pen (color 80 80 120)
  drawTab pen (rect 0 0 (width bm) (height bm)) 50 10
  setCostume (morph tab) bm
  setPosition (morph tab) 20 20
  addPart page tab

  tab = (newBox nil (color 0 0 255) 0 0 false)
  setExtent (morph tab) 300 150
  setGrabRule (morph tab) 'handle'
  bm = (newBitmap (width (bounds (morph tab))) (height (bounds (morph tab))))
  pen = (newPen bm)
  setColor pen (color 80 80 180)
  drawTab pen (rect 0 0 (width bm) (height bm)) 50 10 true
  setCostume (morph tab) bm
  setPosition (morph tab) 120 120
  addPart page tab

  startStepping page
}

demo
