call (function {
  m = (loadModule 'modules/DAVDirectory.gpm')

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u 'tinlizzie.org'
  setPort u 80
  setPath u (array 'gpp' 'test.txt')

  d = (new (at m 'DAVClient'))
  openURL d u
  put d 'testdesus'
})
