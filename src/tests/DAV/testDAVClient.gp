call (function {
  m = (loadModule 'modules/DAVDirectory.gpm')

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u 'tinlizzie.org'
  setPort u 80
  setPath u (array 'gpp' 'a')

  assert (toString u) 'http://tinlizzie.org:80/gpp/a'

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u 'tinlizzie.org'
  setPort u 80
  setPath u (array 'gpp')

  assert (toString u) 'http://tinlizzie.org:80/gpp'

  u = (new (at m 'URL'))
  setScheme u 'http'
  setHost u 'tinlizzie.org'
  setPort u 8000
  setPath u (array '')

  assert (toString u) 'http://tinlizzie.org:8000/'

  d = (new (at m 'DAVClient'))
  openURL d u
  print (listFiles d u)
})
