// Morphic scroll frame resizing and dragging demo and test case

to scrollDemo {
  page = (newPage 800 800)
  open page

  t = (newBox nil (color 200 200 255) 0 0 false)
  setExtent (morph t) 150 150
  sf = (scrollFrame t (color 255 200 200))
  setGrabRule (morph sf) 'handle'
  setPosition (morph sf) 50 50
  setExtent (morph sf) 100 100
  // setClipping (morph sf) false
  resizeHandle sf
  addPart (morph page) (morph sf)

  startStepping page
}

scrollDemo