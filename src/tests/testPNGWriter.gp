call (function {
  b = (newBinaryData (* 32 32 4))
  for i ((byteCount b) / 4) {
    byteAtPut b (((i - 1) * 4) + 1) 255
    byteAtPut b (((i - 1) * 4) + 2) 128
    byteAtPut b (((i - 1) * 4) + 3) 64
    byteAtPut b (((i - 1) * 4) + 4) 32
  }

  b = (new 'Bitmap' 32 32 b)
  data = (writeImage (new 'PNGWriter') b)

  expected = (array 137 80 78 71 13 10 26 10 0 0 0 13 73 72 68 82 0 0 0 32 0 0 0 32 8 6 0 0 0 115 122 122 244 0 0 0 47 73 68 65 84 120 156 237 206 33 1 0 0 8 3 48 162 16 133 104 52 135 24 55 19 243 171 217 235 164 18 16 16 16 16 16 16 16 16 16 16 16 16 16 16 72 7 30 27 117 124 106 79 172 247 189 0 0 0 0 73 69 78 68 174 66 96 130)

  assert (byteCount data) (count expected) 'count PNG'
  assert (toArray data) expected 'PNG data'

})
