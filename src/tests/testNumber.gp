call (function {
x = 1
y = 2

assert x 1 'x'
assert 1 x '1'
assert y 2 'y'
assert 2 y '2'

//expect (1 = y) error
//expect (2 = x) error
})
