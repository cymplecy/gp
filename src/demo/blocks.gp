// Blocks drawing demo

method drawBlock Pen x y width labelString blockColor corner dent inset border height {
  if (isNil corner) {corner = 20}
  if (isNil dent) {dent = 35}
  if (isNil inset) {inset = 50}
  if (isNil border) {border = 10}
  if (isNil height) {height = 120}

  oldColor = color
  oldSize = size
  color = blockColor
  bounds = (rect x y width height)

  r = (max 1 ((border / 2) - 1))
  left = (left bounds)
  top = (top bounds)
  right = (right bounds)
  bottom = (bottom bounds)

  // top
  fillCircle this (left + corner) (top + corner) corner
  fillRect this (left + corner) top (inset - corner) corner
  fillBottomLeftCorner this (rect (left + inset) top corner corner)
  fillBottomRightCorner this (rect (((left + inset) + dent) + corner) top corner corner)
  l = (+ left inset dent (corner * 2))
  fillRect this l top ((right - l) - corner) corner
  fillCircle this ((right - corner) - 1) (top + corner) corner

  // body
  fillRect this left (top + corner) (width bounds) ((height bounds) - (corner * 3))

  // bottom
  fillCircle this (left + corner) ((bottom - (corner * 2)) - 1) corner
  fillRect this (left + corner) (bottom - (corner * 2)) ((width bounds) - (corner * 2)) corner
  fillCircle this ((right - corner) - 1) ((bottom - (corner * 2)) - 1) corner

  // bottom dent
  fillTopRightCorner this (rect (left + inset) (bottom - corner) corner corner)
  fillRect this (+ left inset corner) (bottom - corner) dent corner

  fillTopLeftCorner this (rect (+ left inset dent corner) (bottom - corner) corner corner)

  // 3D edges
  bodyColor = color
  size = (r * 2)

  // top
  color = (lighter color)
  fillRoundedRect this (rect (left + (corner - r)) top ((inset - corner) + r) border) r
  fillRoundedRect this (rect ((left + inset) + (corner - (size - 1))) (top + corner) (dent + size) border) r
  drawLine this (+ left (inset - r)) (+ top r) (+ left inset (corner - r)) (+ top corner r)
  fillRoundedRect this (rect l top (((right - l) - corner) + r) border) r

  // left
  fillRoundedRect this (rect left (top + (corner - r)) border (((height bounds) - (corner * 3)) + border)) r
  drawTopLeftCircleQuadrant this (+ left corner r) (+ top corner r) corner


  // bottom
  color = (darker bodyColor)
  fillRoundedRect this (rect (left + (corner - r)) ((bottom - corner) - border) ((inset - corner) + r) border) r
  fillRoundedRect this (rect ((left + inset) + corner) (bottom - border) dent border) r
  drawLine this (+ left inset corner dent) (- bottom r) (+ left inset corner dent corner) (bottom - (+ corner r))
  fillRoundedRect this (rect (l - (r - 1)) ((bottom - corner) - border) (((right - l) - corner) + size) border) r

  // right
  fillRoundedRect this (rect (right - border) (top + (corner - r)) border (((height bounds) - (corner * 3)) + border)) r
  drawBottomRightCircleQuadrant this (- right (+ corner r)) (- bottom (+ (corner * 2) r)) corner

  color = bodyColor

  // label
  offset = (max (height / 50) 1)
  setFont 'Arial Black' (((height bounds) * 3) / 8)
  if (isClass canvas 'Bitmap') {
	drawString canvas labelString (darker color) (left + corner) ((top + corner) + r)
	drawString canvas labelString (color 255 255 255) (left + (corner + offset)) ((top + (corner + offset)) + r)
  } else {
	c = (copy (darker color))
	setAlpha c 0
	bm = (newBitmap (stringWidth labelString) (fontHeight) c)
	drawString bm labelString (darker color)
	drawBitmap canvas bm (left + corner) ((top + corner) + r)
	fill bm (color 255 255 255 0)
	drawString bm labelString (gray 255)
	drawBitmap canvas bm (left + (corner + offset)) ((top + (corner + offset)) + r)
  }
  color = oldColor
  size = oldSize
}

defineClass AnimatedBlock texture x y r dx dy dr

to newAnimatedBlock texture x y noTumble {
  dx = (rand -10 10)
  dy = (rand -10 10)
  r = 0
  if noTumble {
    dr = 0
  } else {
    dr = (rand -3 3)
  }
  if (and (dx == 0) (dy == 0)) { dx = 1; dy = -1 }
  return (new 'AnimatedBlock' texture x y r dx dy dr)
}

to animatedBlock label color size scale isBitmap {
  if (label == nil) {label = 'foo'}
  if (size == nil) {size = 3}
  if (scale == nil) {scale = 1}
  corner = (scale * 3)
  dent = (scale * 8)
  inset = (scale * 9)
  border = (scale * 2)
  height = (scale * 19)
  width = (* 5 (max size 8) scale)
  if isBitmap {
    canvas = (newBitmap width height)
  } else {
    canvas = (newTexture width height)
  }
  drawBlock (newPen canvas) 0 0 width label color corner dent inset border height
  return (newAnimatedBlock canvas 100 100 isBitmap)
}

method place AnimatedBlock left top {
  x = left
  y = top
}

method step AnimatedBlock {
  // bounce: if outside of box, ensure direction is inward
  if (x < -100) { dx = (abs dx) }
  if (y < -100) { dy = (abs dy) }
  if (x > 970) { dx = (0 - (abs dx)) }
  if (y > 800) { dy = (0 - (abs dy)) }
  x += dx
  y += dy
  r += dr
  if (isClass texture 'Texture') {
    showTexture nil texture x y 240 1 1 r
  } else {
    drawBitmap nil texture x y 240
  }
}

to shapes {
  openWindow
  pen = (newPen)
  drawLine pen 10 10 80 400
  drawLine pen 50 100 420 130
  drawCircle pen 50 50 30
  drawCircle pen 200 200 170
  fillCircle pen 130 130 50
  fillBottomLeftCorner pen (rect 300 0 100 80)
  fillBottomRightCorner pen (rect 300 100 100 80)
  fillTopLeftCorner pen (rect 300 200 100 80)
  fillTopRightCorner pen (rect 300 300 100 80)
  fillRoundedRect pen (rect 40 200 150 100) 20

  setLineWidth pen 8
  drawLine pen 20 400 250 300
  flipBuffer
}

to blocks {
  openWindow
  pen = (newPen)
  drawBlock pen 20 20 292 'GP Rocks!' (color 74 108 212)
  drawBlock pen 20 120 380 'John Maloney' (color 230 168 34)
  drawBlock pen 20 220 450 'Yoshiki Ohshima' (color 143 86 227)
  drawBlock pen 20 320 322 'Jens Mönig' (color 0 161 120)
  flipBuffer
}

to s scale {
  if (scale == nil) {scale = 1}
  corner = (scale * 3)
  dent = (scale * 8)
  inset = (scale * 9)
  border = (scale * 2)
  height = (scale * 19)
  next = (height - corner)
  left = 20
  top = 20

  openWindow (20 + (scale * 80)) (20 + (scale * 80))
  pen = (newPen)
  drawBlock pen left top (scale * 48) 'GP Rocks!' (color 74 108 200) corner dent inset border height
  drawBlock pen left (top + next) (scale * 62) 'John Maloney' (color 230 168 34) corner dent inset border height
  drawBlock pen left (top + (next * 2)) (scale * 72) 'Yoshiki Ohshima' (color 143 86 227) corner dent inset border height
  drawBlock pen left (top + (next * 3)) (scale * 53) 'Jens Mönig' (color 0 161 120) corner dent inset border height
  flipBuffer
}

to animate maxScale useBitmap {
  if (maxScale == nil) {maxScale = 10}
  openWindow 1024 768 false 'GP: Big Ideas'
  left = 200
  top = 150
  height = 120
  next = 100
  clrs = (array
    (color 74 108 212)
    (color 143 86 227)
    (color 207 74 217)
    (color 0 161 120)
    (color 230 168 34)
    (color 4 148 220)
    (color 98 194 19)
    (color 243 118 29)
    (color 217 77 17)
    (color 150 150 150)
  )
  clrCount = (count clrs)
  lbls = (array
    (array 'Scratch' 7)
    (array 'Squeak' 6)
    (array 'BYOB' 4)
    (array 'Morphic' 7)
    (array 'Lively' 6)
    (array 'Snap!' 4)
    (array 'KSWorld' 7)
    (array 'Etoys' 5)
    (array 'Smalltalk' 9)
    (array 'LOGO' 4)
    (array 'Croquet' 7)
    (array 'SELF' 4)
    (array 'Objects' 7)
    (array 'Blocks' 6)
    (array 'Fabrik' 6)
    (array 'Turtles' 7)
    (array 'Media' 5)
    (array 'Music' 5)
    (array 'Art' 3)
    (array 'Create' 6)
    (array 'Invent' 6)
    (array 'Learn' 5)
    (array 'Inspire' 7)
    (array 'Explore' 7)
    (array 'Discover' 8)
    (array 'Share' 5)
    (array 'Imagine' 7)
    (array 'Reflect' 7)
    (array 'Interact' 8)
    (array 'Design' 6)
    (array 'GP' 2)
  )
  lblCount = (count lbls)
  blocks = (list)
  canvas = (newTexture 292 height)
  drawBlock (newPen canvas) 0 0 292 'GP Rocks!' (color 74 108 212)
  showTexture nil canvas left top
  addFirst blocks (newAnimatedBlock canvas left top true)
  top += next
  canvas = (newTexture 380 height)
  drawBlock (newPen canvas) 0 0 380 'John Maloney' (color 230 168 34)
  showTexture nil canvas left top
  addFirst blocks (newAnimatedBlock canvas left top true)
  top += next
  canvas = (newTexture 450 height)
  drawBlock (newPen canvas) 0 0 450 'Yoshiki Ohshima' (color 143 86 227)
  showTexture nil canvas left top
  addFirst blocks (newAnimatedBlock canvas left top true)
  top += next
  canvas = (newTexture 322 height)
  drawBlock (newPen canvas) 0 0 322 'Jens Mönig' (color 0 161 120)
  showTexture nil canvas left top
  addFirst blocks (newAnimatedBlock canvas left top true)
  flipBuffer
  sleep 2000
  while true {
    clearBuffer
    if (mouseDown) {
      end = 5
      if (maxScale < 4) {end = 100}
      for i end {
      pair = (at lbls (rand 1 lblCount))
      nb = (animatedBlock (at pair 1) (at clrs (rand 1 clrCount)) (at pair 2) (rand 1 (min 50 maxScale)) useBitmap)
      place nb (mouseX) (mouseY)
      add blocks nb
      bc = (count blocks)
      if ((bc % 100) == 0) {
        print bc 'blocks'
      }
      }
    }
    for i (count blocks) {
      step (at blocks i)
    }
    flipBuffer
  }
}

blocks