defineClass FingerTrackDemo fingers

// On Mac Powerbook: you get both mouse and touch events (mouse 'which' is 0)
// On iOS simulator: you get both mouse and touch events (mouse 'which' is -1)

method run FingerTrackDemo {
  fingers = (dictionary)
  openWindow
  while true {
    processEvents this
	updateDisplay this
  }
}

method processEvents FingerTrackDemo {
  winW = (at (windowSize) 3)
  winH = (at (windowSize) 4)
  e = (nextEvent)
  if (isNil e) {
    sleep 10
	return
  }
  while (notNil e) {
    type = (at e 'type')
    if ('touchdown' == type) {
	  atPut fingers (at e 'fingerID') (array (randomColor) (winW * (at e 'x')) (winH * (at e 'y')))
    }
    if ('touchup' == type) {
	  remove fingers (at e 'fingerID')
    }
    if ('touchmove' == type) {
     moveFinger this (at e 'fingerID') (winW * (at e 'x')) (winH * (at e 'y'))
    }
    e = (nextEvent)
  }
}

method moveFinger FingerTrackDemo id x y {
  f = (at fingers id)
  if (isNil f) {
    f = (array (randomColor) x y)
	atPut fingers id f
  }
  atPut f 2 x
  atPut f 3 y
}

method updateDisplay FingerTrackDemo {
  pen = (newPen)
  clearBuffer
  for f (values fingers) {
    setColor pen (at f 1)
	fillCircle pen (truncate (at f 2)) (truncate (at f 3)) 20
  }
  flipBuffer
}

run (new 'FingerTrackDemo')
