defineClass Note key beats
defineClass Staff notes

to newStaff {
  return (new 'Staff' (list))
}

method addNote Staff key beat {
  add notes (new 'Note' key beat)
}

method asVoice Staff waitBeats channel tempo volume {
  voice = (newArray (count notes))
  now = ((60000 / tempo) * waitBeats)
  for i (count notes) {
    note = (at notes i)
    duration = ((60000 / tempo) * (getField note 'beats'))
    atPut voice i (new 'NoteOn' now (getField note 'key') volume channel (duration - 10))
    now += duration
  }
  return voice
}

to newMelody {
  melody = (newStaff)
  addNote melody 60 4
  addNote melody 62 4
  addNote melody 67 4
  addNote melody 60 4
  addNote melody 67 4
  addNote melody 69 2
  addNote melody 65 2
  addNote melody 67 4
  addNote melody 60 4
  addNote melody 76 2
  addNote melody 76 1
  addNote melody 76 1
  addNote melody 79 1
  addNote melody 77 1
  addNote melody 77 2
  addNote melody 74 3
  addNote melody 74 1
  addNote melody 77 1
  addNote melody 76 1
  addNote melody 76 2
  addNote melody 72 2
  addNote melody 72 1
  addNote melody 72 1
  addNote melody 76 1
  addNote melody 74 1
  addNote melody 74 2
  addNote melody 71 2
  addNote melody 71 1
  addNote melody 74 1
  addNote melody 72 4
  addNote melody 72 2
  addNote melody 72 1
  addNote melody 72 1
  addNote melody 76 1
  addNote melody 74 1
  addNote melody 74 2
  addNote melody 71 3
  addNote melody 71 1
  addNote melody 74 1
  addNote melody 72 1
  addNote melody 72 2
  addNote melody 64 2
  addNote melody 64 1
  addNote melody 64 1
  addNote melody 62 1
  addNote melody 65 1
  addNote melody 69 2
  addNote melody 67 2
  addNote melody 67 1
  addNote melody 65 1
  addNote melody 64 4
  return melody
}

to playRound {
  round = (newMelody)
  sp = (newScorePlayer)
  addVoice sp (asVoice round 0 1 260 70)
  addVoice sp (asVoice round 32 2 260 70)
  addVoice sp (asVoice round 64 3 260 70)
  play sp
}

playRound
