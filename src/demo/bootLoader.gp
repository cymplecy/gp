// This file can be used to load the GP library files into memory in a "bare"
// GP virtual machine (i.e. when the VM has not loaded the library at startup).
// It isn't normally needed, but it can be useful when boostrapping a new GP VM.

to loadFile f {
  s = (readFile f)
  if (isNil s) {
	log 'File not found:' f
  } else {
	log 'Loading' f
	for cmdList (parse s) {
	  if (isClass cmdList 'Reporter') {
		// Convert reporters into 'return' commands
		cmd = (newIndexable 'Command' 1)
		setField cmd 'primName' 'return'
		setField cmd (objWords cmd) cmdList
		cmdList = cmd
	  }
	  if (notNil cmdList) {
		f = (function cmdList)
		setField f 'module' (topLevelModule)
		call f
	  }
	}
  }
}

to loadLib {
  for f (listFiles 'lib') {
	loadFile (joinStringArray (array 'lib/' f))
  }
}
