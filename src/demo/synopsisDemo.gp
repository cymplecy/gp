to demo {
  page = (newPage 1000 800)
  open page 'GP Synopsis'
  syn = (newSynopsis)
  setPosition (morph syn) 10 10
  addPart (morph page) (morph syn)
  startStepping page
}

demo
