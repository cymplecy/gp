to ometaCompile input output {
  if (isNil input) {
    return (error 'input file name not specified.')
  }
  input = (toList (letters (readFile input)))
  p = (new 'OMetaParser')
  useMemo p
  tree = (match p input 'grammar')
  p = (new 'OMetaTranslator')
  setField p 'level' 0
  comp = (match p (array tree) 'trans')
  code = (joinStringArray (toArray (flattened comp)))
  if (output != nil) {
    writeFile output code
  } else {
    print code
  }
}

to findArgs {
  cmdLine = (commandLine)
  i = (indexOf cmdLine '--')
  if (isNil i) { return (array) }
  return (copyArray cmdLine ((count cmdLine) - i) (i + 1))
}

callWith 'ometaCompile' (findArgs)
