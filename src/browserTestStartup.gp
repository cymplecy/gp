// Start up file for testing GP in the browser.

to browserInteractionLoop {
	print 'Browser interaction loop'
	while true {
		pair = (browserGetDroppedFile)
		if (notNil pair) {
			fName = (callWith 'string' (first pair))
			data = (last pair)
			if (endsWith fName '.gp') {
				(safelyRun (action 'eval' (toString data) nil (topLevelModule)))
			}
		}
		text = (browserGetDroppedText)
		if (notNil text) {
			result = (safelyRun (action 'eval' text nil (topLevelModule)))
			if (isClass result 'Task') {
				printStackTrace result
			} else {
				print result
			}
		}
		waitMSecs 10
	}
}

to startup {
	setSessionModule (initialize (new 'Module') 'SessionModule')
	setGlobal 'alanMode' false
	setGlobal 'flatBlocks' true
	setGlobal 'stealthBlocks' false
	setGlobal 'vectorGraphics' true
	setGlobal 'vectorTrails' false
	setGlobal 'scale' 2

	task = (newTask (newCommand 'browserInteractionLoop'))
	setField task 'tickLimit' 1000000
	resume task
}
