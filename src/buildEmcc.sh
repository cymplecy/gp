#!/bin/sh
#
# Note (old?): Setting ALLOW_MEMORY_GROWTH=1 causes a 2x to 4x slowdown in Firefox on MacBook Pro!
# Note, March 2018: -O3 faster for Firefox & WASM; -Os faster for Safari & Chrome asm.js

emcc -std=gnu99 -Wall -O3 \
-D EMSCRIPTEN \
-D NO_SDL \
-D NO_SOCKETS \
-D SHA2_USE_INTTYPES_H \
-s USE_ZLIB=1 \
-s TOTAL_MEMORY=117440512 \
-s ALLOW_MEMORY_GROWTH=0 \
--memory-init-file 0 \
-s WASM=1 \
browserPrims.c cache.c dict.c embeddedFS.c events.c gp.c interp.c jpegPrims.c mem.c memGC.c \
oop.c parse.c prims.c serialPortPrims.c sha1.c sha2.c soundPrims.c textAndFontPrims.c vectorPrims.c \
jpeglib.bc \
--preload-file runtime/lib \
--preload-file runtime/modules \
--preload-file runtime/startup.gp \
--preload-file runtime/instruments \
--preload-file runtime/midi \
--preload-file runtime/parts \
--preload-file Examples \
--preload-file Downloads \
-o gp_wasm.html

rm -f gp_js.html gp_wasm.html

# Use -Os for asm.js
#-o gp_js.html

# Use -O3 for wasm
#-s WASM=1 \
#-o gp_wasm.html

# Memory size (TOTAL_MEMORY) notes:
# 268435456 = 256MB -- this stopped working on iOS 10 Chrome and Safari
# 209715200 = 200MB -- still fails on ios Chrome
# 188743680 = 180MB -- still fails on ios Chrome
# 157286400 = 150MB -- loads first time on Chrome, but fails on reload
# 136314880 = 130MB -- loads and reloads on Chrome, sometimes forces reload on Safari
# 125829120 = 120MB -- loads and reloads on Chrome, sometimes forces reload on Safari
# 117440512 = 112MB -- decreased from 120MB; new Emscripten compiler wants a multiple of 16MB
# 104857600 = 100MB -- sometimes forces reload on Safari
# 62914560 = 80MB -- sometimes forces reload on Safari (not as often as 100MB, but it happens)
# 62914560 = 60MB -- reliable on Safari

# The following options can help with debugging:
#-s ASSERTIONS=2 \
#-s SAFE_HEAP=1 \
