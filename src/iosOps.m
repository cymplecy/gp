// iosOps.c - Functions to support iOS
// John Maloney, October 2016

#include <CoreFoundation/CoreFoundation.h>
#include <Foundation/Foundation.h>
#define NSDirectoryEnumerationSkipsHiddenFiles 2

#include <UIKit/UIApplication.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mem.h"
#include "interp.h"
#include "parse.h"

// ***** Variables *****

#define MAX_PATH 5000
static char bundlePath[MAX_PATH] = {0};
static char documentsPath[MAX_PATH] = {0};
static int initialized = false;

static int ios_setupDocumentsDirctory() {
	CFArrayRef paths = (CFArrayRef)NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	id documentsDirectory = NULL;

	if (paths) {
		if (CFArrayGetCount(paths) > 0) {
			documentsDirectory = (id)CFArrayGetValueAtIndex(paths, 0);
		}
	}

	if (!documentsDirectory) {return 0;}

	return 1; // for now, don't create subfolders in the docs directory

	NSError * error;
	NSArray *array = [NSArray arrayWithObjects:@"/instruments", @"/lib", @"/midi", @"/modules", nil];

	for (int i = 0; i < [array count]; i++) {
		NSString *dirName = [array objectAtIndex: i];
		NSString *dataPath = [documentsDirectory stringByAppendingPathComponent: dirName];
		if (![[NSFileManager defaultManager] fileExistsAtPath: dataPath])
			[[NSFileManager defaultManager] createDirectoryAtPath: dataPath withIntermediateDirectories: NO attributes: nil error: &error]; //Create folder
	}
	return 1;
}

static void initPaths() {
	// Initialize and cache paths to document and bundle folders (neither ends in a '/').

	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	strncpy(bundlePath, [[[NSBundle mainBundle] resourcePath] UTF8String], MAX_PATH);
	strncpy(documentsPath, [[searchPaths objectAtIndex: 0] UTF8String], MAX_PATH);
	bundlePath[MAX_PATH - 1] = 0; // ensure null termination
	documentsPath[MAX_PATH - 1] = 0; // ensure null termination
	ios_setupDocumentsDirctory();

	initialized = true;
}

void ios_fullPath(char *gpPath, char *result, int resultSize) {
	// Convert a GP path into a full iOS path. Absolute GP paths (i.e. starting with '/')
	// refer to read-only files in the application bundle. Relative GP paths refer to
	// files in the writeable (and iTunes-visible) application documents folder.

	if (!initialized) initPaths();

	result[0] = 0; // clear result
	if ('/' == gpPath[0]) { // absolute path: bundle file system
		strncpy(result, bundlePath, resultSize);
		if (strlen(gpPath) > 1) strncat(result, gpPath, resultSize);
	} else { // relative path: application documents folder
		strncpy(result, documentsPath, resultSize);
		if (strlen(gpPath) > 0) {
			strncat(result, "/", resultSize);
			strncat(result, gpPath, resultSize);
		}
	}
	result[resultSize - 1] = 0; // ensure null termination
}

OBJ ios_absolutePath(char *path) {
	// Convert the given path to an absolute path in GP's simulated file hierarchy.
	// Note: The realpath() function is used to resolve relative paths (i.e. use of "..").

	if (!initialized) initPaths();

	char fullPath[5000] = {0};
	char absolutePath[5000] = {0};
	char result[5000] = {0};

	ios_fullPath(path, fullPath, 5000);
	realpath(fullPath, absolutePath);

	// Note: Under iOS running on an actual device (vs. the simulator), realpath prefixes
	// the absolute path with the string '/private'. Thus, we cannot assume that the bundle
	// or document path match occurs right at the exact beginning of the absolute path.
	char *bundleMatch = strstr(absolutePath, bundlePath);
	char *documentsMatch = strstr(absolutePath, documentsPath);

	result[0] = 0; // clear result
	if (bundleMatch) {
		strncpy(result,  "/", 5000);
		strncat(result, (bundleMatch + strlen(bundlePath) + 1), 5000);
	} else if (documentsMatch) {
		strncpy(result, (documentsMatch + strlen(documentsPath) + 1), 5000);
	} else {
		if ('/' == path[0]) strncpy(result, "/", 5000);
	}
	return newString(result);
}

OBJ ios_directoryContents(char *dirName, gp_boolean listDirectories) {
	// Return an Array containing the contents of the given directory directory.
	// If listDirectories is true, return subdirectory names, otherwise return file names.

	char fullPath[5000];
	ios_fullPath(dirName, fullPath, 5000);
	int len = strlen(fullPath);
	if ((0 == len) || ('/' != fullPath[len - 1])) strncat(fullPath, "/", 5000); // ensure final slash

	NSMutableString *nsPath = [[NSMutableString string] initWithUTF8String: fullPath];
	id mgr = [NSFileManager defaultManager];
	NSArray *items = [mgr contentsOfDirectoryAtPath: nsPath error: NULL];

	int itemCount = [items count];
	OBJ result = newArray(itemCount); // counts both files and dirs, so may be more than needed
	int i = 0;
	BOOL exists, isDirectory;
	for (NSString *item in items) {
		exists = [mgr fileExistsAtPath: [nsPath stringByAppendingString: item] isDirectory: &isDirectory];
		if (exists && (isDirectory == listDirectories) && (i < itemCount)) {
			FIELD(result, i++) = newString((char *) [item UTF8String]);
		}
	}
	if (i < itemCount) result = copyObj(result, i, 1); // remove unused slots from result
	return result;
}

// Library loading support for iOS

#define MAX_FILE_SIZE 1000000
static char buffer[MAX_FILE_SIZE];

static void runItemInBundle(CFURLRef url) {
	CFReadStreamRef stream = CFReadStreamCreateWithFile(NULL, url);
	if (!CFReadStreamOpen(stream)) {
		CFRelease(stream);
		return;
	}
	int byteCount = CFReadStreamRead(stream, (UInt8 *) buffer, MAX_FILE_SIZE);
	char fName[1024];
	CFStringRef path = CFURLCopyLastPathComponent(url);
	CFStringGetCString(path, fName, 1024, kCFStringEncodingUTF8);
	CFRelease(path);
	CFRelease(stream);
	parse_runScriptsInBuffer(fName, buffer, byteCount);
}

void ios_loadLibrary() {
	CFBundleRef mainBundle = CFBundleGetMainBundle();
	CFArrayRef libs = CFBundleCopyResourceURLsOfType(mainBundle, CFSTR("gp"), CFSTR("runtime/lib"));
	CFIndex count = CFArrayGetCount(libs);
	for (int i = 0; i < count; i++) {
		CFURLRef url = (CFURLRef) CFArrayGetValueAtIndex(libs, i);
		runItemInBundle(url);
	}
	CFRelease(libs);
}

NSArray* ios_filesInDirectory(NSString *path) {
  NSFileManager *manager = [NSFileManager defaultManager];
  return [manager
		 contentsOfDirectoryAtURL: [NSURL URLWithString: [@"file://" stringByAppendingString: path]]
		 includingPropertiesForKeys: nil
		 options: NSDirectoryEnumerationSkipsHiddenFiles
		 error: nil];
}

static int endsWith(char *buf, char *suffix) {
  int len = strlen(buf);
  int slen = strlen(suffix);
  if (slen > len) {return 0;}
  for (int i = 0; i < slen; i++) {
    if (buf[len - 1 - i] != suffix[slen - 1 - i]) {
      return 0;
    }
  }
  return 1;
}

void ios_loadUserFilesAndMainFile() {
	CFArrayRef paths = (CFArrayRef)NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

	unsigned int count = 0;
	id documentsDirectory = NULL;

	CFArrayRef elems;
	if (paths) {
		if (CFArrayGetCount(paths) > 0) {
			documentsDirectory = (id)CFArrayGetValueAtIndex(paths, 0);
			elems = (CFArrayRef)ios_filesInDirectory(documentsDirectory);
			count = CFArrayGetCount(elems);
		}
	}
	char *startupFileName = "/startup.gp";
	char buf[1024];
	char mainBuf[1024] = {'\0'};
	for (int i = 0; i < count; i++) {
		CFURLRef elem = CFArrayGetValueAtIndex(elems, i);
		CFURLGetFileSystemRepresentation(elem, NO, (unsigned char*)buf, 1024);
		char *c = buf + strlen(buf) - strlen(startupFileName);
		if (strncmp(startupFileName, c, strlen(startupFileName)) == 0) {
			strcpy(mainBuf, buf);
		} else if (endsWith(buf, ".gp")) {
			parse_runFile(buf);
		}
	}
	if (strlen(mainBuf) > 0) {
		parse_runFile(mainBuf);
		return;
	}
	CFBundleRef mainBundle = CFBundleGetMainBundle();
	CFURLRef url = CFBundleCopyResourceURL(mainBundle, CFSTR("runtime/startup.gp"), NULL, NULL);
	runItemInBundle(url);
	CFRelease(url);
	return;
}

void ios_hideStatusBar() {
	// Note: The "setStatusBarHidden" function became deprecated in iOS 9 and now does nothing.
	id uiApp = [UIApplication sharedApplication];
	[uiApp setStatusBarHidden:YES withAnimation: UIStatusBarAnimationFade];

	/* earlier attempts, none of which work: */
// 	[uiApp setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:UIStatusBarAnimationFade];
// 	object_setInstanceVariable(uiApp, "statusBarHidden", YES);
}
