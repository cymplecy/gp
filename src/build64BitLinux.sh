#!/bin/sh
# Build on 64-bit Ubuntu 16.04
# Installed packages: libcairo2-dev libpango1.0-dev libasound-dev
# Built and installed from source: SDL2.0.5, jpeg-8d1, and portaudio

gcc -std=gnu99 -Wall -Wno-strict-aliasing -O3 \
  -I/usr/local/include/SDL2 \
  `pkg-config --cflags pangocairo` \
  cache.c dict.c embeddedFS.c events.c gp.c graphicsPrims.c interp.c jpegPrims.c mem.c memGC.c oop.c \
  parse.c prims.c serialPortPrims.c sha1.c sha2.c socketPrims.c soundPrims.c textAndFontPrims.c vectorPrims.c \
  /usr/local/lib/libSDL2.a \
  /usr/local/lib/libjpeg.a \
  /usr/local/lib/libportaudio.a -lasound -ljack \
  `pkg-config --libs pangocairo` \
  -lm -ldl -lz -pthread \
  -o gp-linux64bit

