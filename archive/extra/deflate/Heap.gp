defineClass Heap array tally

to heap capacity {
  if (isNil capacity) {capacity = 10}
  return (new 'Heap' (newArray capacity) 0)
}

method toString Heap size {
  if (isNil size) {size = 1000}
  list = (list)
  str = 'Heap('
  size += (0 - (byteCount str))
  add list str
  for i tally {
    elem = (at array i)
    str = (toString elem)
    size += (0 - (byteCount str))
    if (size > 0) {
      add list str
      if (i < tally) {
        add list ' '
      }
    } else {
      add list ' ...)'
      return (joinStringArray (toArray list))
    }
  }
  add list ')'
  return (joinStringArray (toArray list))
}

method count Heap { return tally }

method add Heap anObject {
  // Include newObject as one of the receiver's elements. Answer newObject.
  if (tally == (count array)) {grow this}
  tally += 1
  atPut array tally anObject
  upHeap this tally
  return anObject
}

method grow Heap {
  newSize = (tally + (max (count array) 5))
  array = (copyArray array newSize)
}

method at Heap index {
  // Return the element at the given position within the receiver
  if (or (index < 1) (index > tally)) {
    error (join 'subscript out of bounds' (toString index))
  }
  return (at array index)
}

method removeFirst Heap {
  // Remove the first element
  return (removeAt this 1)
}

method removeAt Heap index {
  // Remove the element at the given index and make sure the sorting order is okay

  if (or (index < 1) (index > tally)) { error 'index out of bounds' }

  removed = (at array index)
  atPut array index (at array tally)
  atPut array tally nil
  tally += -1
  if (not (index > tally)) {
    downHeapSingle this index
  }
  return removed
}

method downHeap Heap anIndex {
  // Check the heap downwards for correctness starting at anIndex.
  //  Everything above (i.e. left of) anIndex is ok.
  if (anIndex == 0) {return}
  n = (tally >> 1)
  k = anIndex
  value = (at array anIndex)
  while (k <= n) {
    j = (k + k)
    if (and (j < tally) ((at array (j + 1)) <= (at array j))) {
      j += 1
    }
    // check if position k is ok
    if (value <= (at array j)) {
      // yes -> break loop
      n = (k - 1)
    } else {
      // no -> make room at j by moving j-th element to k-th position
      atPut array k (at array j)
      // and try again with j
      k = j
    }
  }
  atPut array k value
}

method downHeapSingle Heap anIndex {
  // This version is optimized for the case when only one element in
  // the receiver can be at a wrong position. It avoids one comparison at
  // each node when travelling down the heap and checks the heap upwards
  // after the element is at a bottom position. Since the probability for
  // being at the bottom of the heap is much larger than for being
  // somewhere in the middle this version should be faster.

  if (anIndex == 0) {return}
  n = (tally >> 1)
  k = anIndex
  value = (at array anIndex)
  while (k <= n) {
    j = (k + k)
    if (and (j < tally) ((at array (j + 1)) <= (at array j))) {
      j += 1
    }
    atPut array k (at array j)
    // and try again with j
    k = j
  }
  atPut array k value
  upHeap this k
}

method upHeap Heap anIndex {
  // Check the heap upwards for correctness starting at anIndex.
  //  Everything below anIndex is ok.
  if (anIndex == 0) {return}
  k = anIndex
  value = (at array anIndex)
  if (k > 1) {
    kDiv2 = (k >> 1)
    tmp = (at array kDiv2)
  } else {
    tmp = nil
  }
  while (and (notNil tmp) (value <= tmp)) {
    atPut array k tmp
    k = kDiv2
    if (k > 1) {
      kDiv2 = (k >> 1)
      tmp = (at array kDiv2)
    } else {
      tmp = nil
    }
  }
  atPut array k value
}
