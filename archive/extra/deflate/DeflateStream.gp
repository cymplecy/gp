defineClass DeflateStream collection position writeLimit encoder hashHead hashTail hashValue blockStart blockPosition literals distances literalFreq distanceFreq litCount matchCount crc crcType crcPosition bytesWritten isZLib HashBits WindowSize WindowMask MaxMatch HashShift HashMask BitLengthOrder MaxBitLengthCodes MinMatch MatchLengthCodes DistanceCodes StoredBlock FixedBlock DynamicBlock Repeat11To138 Repeat3To10 Repeat3To6 MaxDistance MaxLiteralCodes MaxDistCodes EndBlock MaxBits MaxBitLengthBits NumLiterals FixedLiteralTree FixedDistanceTree ExtraLengthBits  ExtraDistanceBits ExtraBitLengthBits All1

method init DeflateStream {
  HashBits = 15
  WindowSize = 32768
  WindowMask = 32767
  MaxMatch = 258
  HashShift = 5
  HashMask = 32767
  MaxDistance = 32768

  MaxBitLengthCodes = 19
  BitLengthOrder = (array 16 17 18 0 8 7 9 6 10 5 11 4 12 3 13 2 14 1 15)
  MatchLengthCodes = (array 257 258 259 260 261 262 263 264 265 265 266 266 267 267 268 268 269 269 269 269 270 270 270 270 271 271 271 271 272 272 272 272 273 273 273 273 273 273 273 273 274 274 274 274 274 274 274 274 275 275 275 275 275 275 275 275 276 276 276 276 276 276 276 276 277 277 277 277 277 277 277 277 277 277 277 277 277 277 277 277 278 278 278 278 278 278 278 278 278 278 278 278 278 278 278 278 279 279 279 279 279 279 279 279 279 279 279 279 279 279 279 279 280 280 280 280 280 280 280 280 280 280 280 280 280 280 280 280 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284)
  DistanceCodes = (array 0 1 2 3 4 4 5 5 6 6 6 6 7 7 7 7 8 8 8 8 8 8 8 8 9 9 9 9 9 9 9 9 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 11 11 11 11 11 11 11 11 11 11 11 11 11 11 11 11 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 0 0 16 17 18 18 19 19 20 20 20 20 21 21 21 21 22 22 22 22 22 22 22 22 23 23 23 23 23 23 23 23 24 24 24 24 24 24 24 24 24 24 24 24 24 24 24 24 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29)

  MaxBits = 15
  MaxBitLengthBits = 7
  MaxLiteralCodes = 286
  MaxDistCodes = 30
  MinMatch = 3
  EndBlock = 256
  NumLiterals = 256

  StoredBlock = 0
  FixedBlock = 1
  DynamicBlock = 2

  Repeat11To138 = 18
  Repeat3To10 = 17
  Repeat3To6 = 16

  ExtraLengthBits = (array 0 0 0 0 0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 5 5 5 0)
  ExtraDistanceBits = (array 0 0 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13)
  ExtraBitLengthBits = (array 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 3 7)

  FixedLiteralTree = (new 'ZipEncoderTree')
  setField FixedLiteralTree 'maxCode' 287

  counts = (newArray (MaxBits + 1) 0)
  atPut counts (7 + 1) 24
  atPut counts (8 + 1) (144 + 8)
  atPut counts (9 + 1) 112

  nodes = (newArray 288)
  for i 288 {
    atPut nodes i (init (new 'ZipEncoderNode') (i - 1) 0 0)
  }
  for ii 144 {
    i = (ii - 1)
    setBitLength (at nodes (i + 1)) 8
  }
  for ii 112 {
    i = (ii + 143)
    setBitLength (at nodes (i + 1)) 9
  }
  for ii 24 {
    i = (ii + 255)
    setBitLength (at nodes (i + 1)) 7
  }
  for ii 8 {
    i = (ii + 279)
    setBitLength (at nodes (i + 1)) 8
  }
  buildCodes FixedLiteralTree nodes counts MaxBits
  setValuesFrom FixedLiteralTree nodes

  FixedDistanceTree = (new 'ZipEncoderTree')
  setField FixedDistanceTree 'maxCode' MaxDistCodes

  codes = (newArray (MaxDistCodes + 1))
  for ii (MaxDistCodes + 1) {
    i = (ii - 1)
    atPut codes (i + 1) (reverseBits FixedDistanceTree i 5)
  }
  setBitLengths FixedDistanceTree (newArray (MaxDistCodes + 1) 5) codes
  All1 = (((toLargeInteger 255) << 24) | 16777215)
}

method contents DeflateStream {
  return (contents encoder)
}

method initializeNewBlock DeflateStream {
  // Initialize the encoder for a new block of data
  fillArray literalFreq 0
  fillArray distanceFreq 0
  atPut literalFreq (EndBlock + 1) 1
  litCount = 0
  matchCount = 0
}

method onZLib DeflateStream aStream {
  on this aStream
  isZLib = true
  zLibWriteHeader this
  crc = 1
  setCrcType this 'adler32'
  return this
}

method setCrcType DeflateStream aString {
  crcType = aString
}

method on DeflateStream aStream {
  init this
  position = 0
  collection = (newBinaryData 65536)
  writeLimit = (byteCount collection)
  crc = (((toLargeInteger 255) << 24) | 16777215)
  crcPosition = 1
  bytesWritten = 0
  encoder = (on (new 'ZipEncoder') aStream)

  blockStart = nil
  blockPosition = 0
  hashValue = 0
  hashHead = (new 'UInt32Array' (newBinaryData ((1 << HashBits) * 4)))
  hashTail = (new 'UInt32Array' (newBinaryData (WindowSize * 4)))

  literals = (newBinaryData WindowSize)
  distances = (new 'UInt32Array' (newBinaryData (WindowSize * 4)))
  literalFreq = (new 'UInt32Array' (newBinaryData (MaxLiteralCodes * 4)))
  distanceFreq = (new 'UInt32Array' (newBinaryData (MaxDistCodes * 4)))
  initializeNewBlock this
  return this
}

method close DeflateStream {
  doDeflateBlock this
  flushBlock this true
  close encoder
}

method release DeflateStream {
  // We're done with compression. Do some cleanup.
  literals = nil
  distances = nil
  literalFreq = nil
  distanceFreq = nil
  updateCrc this
  flushBits encoder
  if (isZLib == true) {
    zLibWriteFooter this
  }
}

method compare DeflateStream here matchPos minLength {
  // Compare the two strings and return the length of matching characters.
  // minLength is a lower bound for match lengths that will be accepted.
  // Note: here and matchPos are zero based.

  // First test if we can actually get longer than minLength
  if (not ((byteAt collection ((here + minLength) + 1)) == (byteAt collection ((matchPos + minLength) + 1)))) {return 0}
  if (not ((byteAt collection (here + minLength)) == (byteAt collection (matchPos + minLength)))) {return 0}
  // Then test if we have an initial match at all
  if (not ((byteAt collection (here + 1)) == (byteAt collection (matchPos + 1)))) {return 0}
  if (not ((byteAt collection (here + 2)) == (byteAt collection (matchPos + 2)))) {return 1}
  // Finally do the real comparison
  length = 3
  while (and (length <= MaxMatch) ((byteAt collection (here + length)) == (byteAt collection (matchPos + length)))) {
    length += 1
  }
  return (length - 1)
}

method doDeflateBlock DeflateStream {
  // Deflate the current contents of the stream
  if (isNil blockStart) {
    // One time initialization for the first block
    for i (MinMatch - 1) {
      updateHashAt this i
    }
    blockStart = 0
  }
  while (blockPosition < position) {
    if ((position + MaxMatch) > writeLimit) {
      lastIndex = (writeLimit - MaxMatch)
    } else {
      lastIndex = position
    }
    flushNeeded = (deflateBlock this (lastIndex - 1) 4096 MaxMatch)
//    flushNeeded = (deflateBlockHelper this (lastIndex - 1) 4096 MaxMatch)
    if flushNeeded {
      flushBlock this
      blockStart = blockPosition
    }
    // Make room for more data
    moveContentsToFront this
  }
}

method deflateBlockHelper DeflateStream lastIndex chainLength goodMatch {
  // Continue deflating the collection from blockPosition to lastIndex.
  // Note that lastIndex must be at least MaxMatch away from the end of collection
  if (blockPosition > lastIndex) {return false} // Nothing to deflate
  hasMatch = false
  here = blockPosition
  while (here <= lastIndex) {
    if (not hasMatch) {
      // Find the first match
      matchResult = (findMatch this here (MinMatch - 1) here chainLength goodMatch)
      insertStringAt this here // update hash table
      hereMatch = (matchResult & 65535)
      hereLength = (matchResult >> 16)
    }

    // Look ahead if there is a better match at the next position
    matchResult = (findMatch this (here + 1) hereLength hereMatch chainLength goodMatch)
    newMatch = (matchResult & 65535)
    newLength = (matchResult >> 16)

    // Now check if the next match is better than the current one.
    // If not, output the current match (provided that the current match
    // is at least MinMatch long)
    if (and (hereLength >= newLength) (hereLength >= MinMatch)) {
      //assert (validateMatchAt this here hereMatch ((hereMatch + hereLength) - 1)) true

      // Encode the current match
      flushNeeded = (encodeMatch this hereLength (here - hereMatch))

      // Insert all strings up to the end of the current match.
      // Note: The first string has already been inserted.
      for i (hereLength - 1) {
        here = (here + 1)
        insertStringAt this here
      }
      hasMatch = false
      here = (here + 1)
    } else {
      // Either the next match is better than the current one or we didn't
      // have a good match after all (e.g., current match length < MinMatch).
      // Output a single literal.

      flushNeeded = (encodeLiteral this (byteAt collection (here + 1)))
      here += 1
      if (and (here <= lastIndex) (not flushNeeded)) {
        // Cache the results for the next round
	insertStringAt this here
	hasMatch = true
	hereMatch = newMatch
	hereLength = newLength
      }
    }
    if flushNeeded {
      blockPosition = here
      return true
    }
  }
  blockPosition = here
  return false
}

method encodeLiteral DeflateStream lit {
  // Encode the given literal
  litCount += 1
  byteAtPut literals litCount lit
  atPut distances litCount 0
  atPut literalFreq (lit + 1) ((at literalFreq (lit + 1)) + 1)
  return (shouldFlush this)
}

method encodeMatch DeflateStream length dist {
  // Encode the given match of length length starting at dist bytes ahead
  if (not (dist > 0)) {error 'Distance must be positive'}
  if (length < MinMatch) {error (join 'Match length must be at least ' (toString MinMatch))}
  litCount += 1
  matchCount += 1
  byteAtPut literals litCount (length - MinMatch)
  atPut distances litCount dist
  literal = (at MatchLengthCodes ((length - MinMatch) + 1))
  atPut literalFreq (literal + 1) ((at literalFreq (literal + 1)) + 1)
  if (dist < 257) {
    distance = (at DistanceCodes dist)
  } else {
    distance = (at DistanceCodes (257 + ((dist - 1) >> 7)))
  }
  atPut distanceFreq (distance + 1) ((at distanceFreq (distance + 1)) + 1)
  return (shouldFlush this)
}

method findMatch DeflateStream here lastLength lastMatch maxChainLength goodMatch {
  // Find the longest match for the string starting at here.
  // If there is no match longer than lastLength return lastMatch/lastLength.
  // Traverse at most maxChainLength entries in the hash table.
  // Stop if a match of at least goodMatch size has been found.

  // Compute the default match result
  matchResult = ((lastLength << 16) | lastMatch)

  // There is no way to find a better match than MaxMatch
  if (lastLength >= MaxMatch) {return matchResult}

  // Start position for searches
  matchPos = (at hashHead ((updateHashAt this (here + MinMatch)) + 1))

  // Compute the distance to the (possible) match
  distance = (here - matchPos)

  // Note: It is required that 0 < distance < MaxDistance
  if (not (and (distance > 0) (distance < MaxDistance))) {return matchResult}

  chainLength = maxChainLength // Max. nr of match chain to search
  if (here > MaxDistance) { // Limit for matches that are too old
    limit = (here - MaxDistance)
  } else {
    limit = 0
  }

  // Best match length so far (current match must be larger to take effect)
  bestLength = lastLength

  while true {
    // Compare the current string with the string at match position
    length = (compare this here matchPos bestLength)
    // Truncate accidental matches beyound stream position
    if ((here + length) > position) {length = (position - here)}
    // Ignore very small matches if they are too far away
    if (and (length == MinMatch) ((here - matchPos) > (MaxDistance / 4))) {
      length = (MinMatch - 1)
    }

    if (length > bestLength) { // We have a new (better) match than before
      // Computer the new match result
      matchResult = ((length << 16) | matchPos)
      bestLength = length
      // There is no way to find a better match than MaxMatch
      if (bestLength >= MaxMatch) {return matchResult}
      // But we may have a good, fast match
      if (bestLength > goodMatch) {return matchResult}
    }
    chainLength = (chainLength - 1)
    if (not (chainLength > 0)) {return matchResult}
    // Compare with previous entry in hash chain
    matchPos = (at hashTail ((matchPos & WindowMask) + 1))
    if (matchPos <= limit) {return matchResult}
  }
}

method fixedBlockSizeFor DeflateStream lTree dTree {
  // Compute the length for the current block using fixed huffman trees
  bits = 3 // block type
  // Compute the size of the compressed block

  for ii (NumLiterals + 1) {
    // encoding of literals
    i = (ii - 1)
    bits = (bits + ((at literalFreq (i + 1)) * (bitLengthAt FixedLiteralTree i)))
  }
  for ii ((maxCode lTree) - NumLiterals) {
    i = (ii + NumLiterals)
    extra = (at ExtraLengthBits (i - NumLiterals))
    bits = (bits + ((at literalFreq (i + 1)) * ((bitLengthAt FixedLiteralTree i) + extra)))
  }
  for ii ((maxCode dTree) + 1) {
    i = (ii - 1)
    extra = (at ExtraDistanceBits (i + 1))
    bits = (bits + ((at distanceFreq (i + 1)) * ((bitLengthAt FixedDistanceTree i) + extra)))
  }
  return bits
}

method flushBlock DeflateStream lastBlock {
  // Send the current block
  if lastBlock {
    lastFlag = 1
  } else {
    lastFlag = 0
  }
  // Compute the literal/length and distance tree
  lTree = (buildTreeFrom (new 'ZipEncoderTree') literalFreq MaxBits)
  dTree = (buildTreeFrom (new 'ZipEncoderTree') distanceFreq MaxBits)

  // Compute the bit length tree
  blBits = (join (bitLengths lTree) (bitLengths dTree))

  blFreq = (new 'UInt32Array' (newBinaryData (MaxBitLengthCodes * 4)))
  scanBitArray this blBits blFreq
  blTree = (buildTreeFrom (new 'ZipEncoderTree') blFreq MaxBitLengthBits)

  // Compute the bit length for the current block.
  // Note: Most of this could be computed on the fly but it's getting
  // really ugly in this case so we do it afterwards.
  storedLength = (storedBlockSize this)
  fixedLength = (fixedBlockSizeFor this lTree dTree)
  dynamicLength = (dynamicBlockSizeFor this lTree dTree blTree blFreq)
//  print 'options' storedLength fixedLength dynamicLength
  // Check which method to use
  method = nil  // (forcedMethod this)
  if (isNil method) {
    if (and (storedLength < fixedLength) (storedLength < dynamicLength)) {
      method = 'stored'
    } (fixedLength < dynamicLength) {
      method = 'fixed'
    } else {
      method = 'dynamic'
    }
  }
  if (and (method == 'stored') (blockStart < 0)) {
    // Cannot use 'stored' if the block is not available
    if (fixedLength < dynamicLength) {
      method = 'fixed'
    } else {
      method = 'dynamic'
    }
  }

  bitsSent = (bitPosition encoder) // number of bits sent before this block
  bitsRequired = nil

//  print 'method (stored, fixed, dynamic)' method storedLength fixedLength dynamicLength

  if (method == 'stored') {
    bitsRequired = storedLength
    nextBitsPut encoder 3 ((StoredBlock << 1) + lastFlag)
    sendStoredBlock this
  } (method == 'fixed') {
    bitsRequired = fixedLength
    nextBitsPut encoder 3 ((FixedBlock << 1) + lastFlag)
    sendFixedBlock this
  } (method == 'dynamic') {
    bitsRequired = dynamicLength
    nextBitsPut encoder 3 ((DynamicBlock << 1) + lastFlag)
    sendDynamicBlock this blTree lTree dTree blBits
  }

  if (not (bitsRequired == ((bitPosition encoder) - bitsSent))) {
    error 'Bits size mismatch'
  }

  if lastBlock {
    release this
  } else {
    initializeNewBlock this
  }
}

method insertStringAt DeflateStream here {
  // Insert the string at the given start position into the hash table.
  // Note: The hash value is updated starting at MinMatch-1 since
  // all strings before have already been inserted into the hash table
  // (and the hash value is updated as well).

  hashValue = (updateHashAt this (here + MinMatch))
  prevEntry = (at hashHead (hashValue + 1))
  atPut hashHead (hashValue + 1) here
  atPut hashTail ((here & WindowMask) + 1) prevEntry
}

method moveContentsToFront DeflateStream {
  // Move the contents of the receiver to the front

  updateCrc this

  delta = (blockPosition - WindowSize)
  if (delta <= 0) {return}

  replaceByteRange collection 1 ((byteCount collection) - delta) collection (delta + 1)
  position = (position - delta)
  // Move hash table entries
  blockPosition = (blockPosition - delta)
  blockStart = (blockStart - delta)
//  updateHashTable this hashHead delta
//  updateHashTable this hashTail delta
  inflateUpdateHashTable this hashHead delta
  inflateUpdateHashTable this hashTail delta

  crcPosition = (position + 1)
}

method nextPutAll DeflateStream aCollection startIndex bytesCount {
  if (isNil startIndex) { startIndex = 1 }
  if (isNil bytesCount) { bytesCount = (byteCount aCollection) }
  start = startIndex
  count = bytesCount
  while (count != 0) {
    if (position == writeLimit) {doDeflateBlock this}
    max = (writeLimit - position)
    if (max > count) {max = count}
    replaceByteRange collection (position + 1) (position + max) aCollection start
    start = (start + max)
    count = (count - max)
    position = (position + max)
  }
  return aCollection
}

method updateHash DeflateStream nextValue {
  // Update the running hash value based on the next input byte.
  // Return the new updated hash value.
  return (((hashValue << HashShift) ^ nextValue) & HashMask)
}

method updateHashAt DeflateStream here {
  // Update the hash value at position here (one based)
  return (updateHash this (byteAt collection here))
}

method updateHashTable DeflateStream table delta {
  for i (count table) {
    // Discard entries that are out of range
    pos = (at table i)
    if (pos >= delta) {
      atPut table i (pos - delta)
    } else {
      atPut table i 0
    }
  }
}

method validateMatchAt DeflateStream pos startPos endPos {
  here = pos
  for ii ((endPos - startPos) + 1) {
    i = (startPos + ii)
    here += 1
    if (not ((byteAt collection i) == (byteAt collection here))) {
      return (error 'Not a match')
    }
  }
  return true
}

method dynamicBlockSizeFor DeflateStream lTree dTree blTree blFreq {
  // Compute the length for the current block using dynamic huffman trees
  bits = ((3 + 5) + 5)  // block type + literal codes length + distance codes length
  // Compute the # of bits for sending the bit length tree
  treeBits = 4 // Max index for bit length tree
  index = MaxBitLengthCodes
  while (index >= 4) {
    if (or (index == 4) ((at blFreq ((at BitLengthOrder index) + 1)) > 0)) {
      treeBits = (treeBits + (index * 3))
      index = -1
    } else {
      index += -1
    }
  }

  // Compute the # of bits for sending the literal/distance tree
  // Note: The frequency are already stored in the blTree
  for ii 16 {
    i = (ii - 1)
    // First, the non-repeating values
    freq = (at blFreq (i + 1))
    if (freq > 0) {
      treeBits = (treeBits + (freq * (bitLengthAt blTree i)))
    }
  }
  // Now the repeating values
  for x 3 {
    i = (x + 15)
    addl = (at (array 2 3 7) x)
    freq = (at blFreq (i + 1))
    if (freq > 0) {
      treeBits = (treeBits + (freq * ((bitLengthAt blTree i) + addl)))
    }
  }

  bits = (bits + treeBits)
  // Compute the size of the compressed block
  for ii (NumLiterals + 1) {
    // encoding of literals
    i = (ii - 1)
    freq = (at literalFreq (i + 1))
    if (freq > 0) {
      bits = (bits + (freq * (bitLengthAt lTree i)))
    }
  }
  for ii ((maxCode lTree) - NumLiterals) {
    i = (ii + NumLiterals)
    freq = (at literalFreq (i + 1))
    extra = (at ExtraLengthBits (i - NumLiterals))
    if (freq > 0) {
      bits = (bits + (freq * ((bitLengthAt lTree i) + extra)))
    }
  }
  for ii ((maxCode dTree) + 1) {
    i = (ii - 1)
    freq = (at distanceFreq (i + 1))
    extra = (at ExtraDistanceBits (i + 1))
    if (freq > 0) {
      bits = (bits + (freq * ((bitLengthAt dTree i) + extra)))
    }
  }
  return bits
}

method scanBitRepeatArray DeflateStream bitLength repeatCount anArray {
  // Update the frequency for the aTree based on the given values
  count = repeatCount
  if (bitLength == 0) {
    while (count >= 11) {
      atPut anArray (Repeat11To138 + 1) ((at anArray (Repeat11To138 + 1)) + 1)
      count = (max (count - 138) 0)
    }
    while (count >= 3) {
      atPut anArray (Repeat3To10 + 1) ((at anArray (Repeat3To10 + 1)) + 1)
      count = (max (count - 10) 0)
    }
    if (count > 0) {
      atPut anArray (bitLength + 1) ((at anArray (bitLength + 1)) + count)
    }
  } else {
    atPut anArray (bitLength + 1) ((at anArray (bitLength + 1)) + 1)
    count = (count - 1)
    while (count >= 3) {
      atPut anArray (Repeat3To6 + 1) ((at anArray (Repeat3To6 + 1)) + 1)
      count = (max (count - 6) 0)
    }
    if (count > 0) {
      atPut anArray (bitLength + 1) ((at anArray (bitLength + 1)) + count)
    }
  }
}

method scanBitArray DeflateStream bits anArray {
  // Scan the trees and determine the frequency of the bit lengths.
  // For repeating codes, emit a repeat count.
  if ((count bits) == 0) {return}
  lastValue = (at bits 1)
  lastCount = 1
  for ii ((count bits) - 1) {
    i = (ii + 1)
    value = (at bits i)
    if (value == lastValue) {
      lastCount = (lastCount + 1)
    } else {
      scanBitRepeatArray this lastValue lastCount anArray
      lastValue = value
      lastCount = 1
    }
  }
  scanBitRepeatArray this lastValue lastCount anArray
}

method sendBitRepeatTree DeflateStream bitLength repeatCount aTree {
  // Send the given bitLength, repeating repeatCount times
  count = repeatCount
  if (bitLength == 0) {
    while (count >= 11) {
      sendBitTree this Repeat11To138 aTree
      nextBitsPut encoder 7 ((min count 138) - 11)
      count = (max (count - 138) 0)
    }
    while (count >= 3) {
      sendBitTree this Repeat3To10 aTree
      nextBitsPut encoder 3 ((min count 10) - 3)
      count = (max (count - 10) 0)
    }
    repeat count {
      sendBitTree this bitLength aTree
    }
  } else {
    sendBitTree this bitLength aTree
    count = (count - 1)
    while (count >= 3) {
      sendBitTree this Repeat3To6 aTree
      nextBitsPut encoder 2 ((min count 6) - 3)
      count = (max (count - 6) 0)
    }
    repeat count {
      sendBitTree this bitLength aTree
    }
  }
}

method sendBitTree DeflateStream bitLength aTree {
  // Send the given bitLength
  nextBitsPut encoder (bitLengthAt aTree bitLength) (codeAt aTree bitLength)
}

method sendBitLiteralTree DeflateStream blTree {
  // Send the bit length tree
  for ii 16 {
    maxIndex = (20 - ii)
    blIndex = (at BitLengthOrder maxIndex)
    if (blIndex <= (maxCode blTree)) {
      bitLength = (bitLengthAt blTree blIndex)
    } else {
      bitLength = 0
    }
    if (or (maxIndex == 4) (bitLength > 0)) {
      nextBitsPut encoder 4 (maxIndex - 4)
      for j maxIndex {
        blIndex = (at BitLengthOrder j)
        if (blIndex <= (maxCode blTree)) {
          bitLength = (bitLengthAt blTree blIndex)
        } else {
          bitLength = 0
        }
        nextBitsPut encoder 3 bitLength
      }
      return
    }
  }
}

method sendCompressedBlock DeflateStream litTree distTree {
  // Send the current block using the encodings from the given literal/length and distance tree
  litData = (newBinaryData litCount)
  replaceByteRange litData 1 litCount literals
  litStream = (dataStream litData)

  distData = (newBinaryData (litCount * 4))
  replaceByteRange distData 1 (litCount * 4) (getField distances 'data')
  distStream = (dataStream distData)

  sum = (sendBlock encoder litStream distStream litTree distTree)
  if (not (sum == (blockPosition - blockStart))) {
    error 'Wrong number of bytes'
  }
}

method sendDynamicBlock DeflateStream blTree lTree dTree bits {
  // Send a block using dynamic huffman trees
  sendLiteralTree this lTree dTree blTree bits
  sendCompressedBlock this lTree dTree
}

method sendFixedBlock DeflateStream {
  // Send a block using fixed huffman trees
  sendCompressedBlock this FixedLiteralTree FixedDistanceTree
}

method sendLiteralTree DeflateStream lTree dTree blTree bits {
  // Send all the trees needed for dynamic huffman tree encoding
  nextBitsPut encoder 5 ((maxCode lTree) - 256)
  nextBitsPut encoder 5 (maxCode dTree)
  sendBitLiteralTree this blTree
  if ((count bits) == 0) {return}
  lastValue = (at bits 1)
  lastCount = 1
  for ii ((count bits) - 1) {
    i = (ii + 1)
    value = (at bits i)
    if (value == lastValue) {
      lastCount = (lastCount + 1)
    } else {
      sendBitRepeatTree this lastValue lastCount blTree
      lastValue = value
      lastCount = 1
    }
  }
  sendBitRepeatTree this lastValue lastCount blTree
}

method sendStoredBlock DeflateStream {
  // Send an uncompressed block
  inBytes = (blockPosition - blockStart)
  flushBits encoder // Skip to byte boundary
  nextBitsPut encoder 16 inBytes
  nextBitsPut encoder 16 (inBytes ^ 65535)
  flushBits encoder
  for i inBytes {
    nextBytePut encoder (byteAt collection (blockStart + i))
  }
}

method shouldFlush DeflateStream {
  // Check if we should flush the current block.
  // Flushing can be useful if the input characteristics change.
  if (litCount == (byteCount literals)) {return true} // We *must* flush
  if (not ((litCount & 4095) == 0)) {return false} // Only check every N kbytes
  if ((matchCount * 10) <= litCount) {
    // This is basically random data. 
    // There is no need to flush early since the overhead
    // for encoding the trees will add to the overall size
    return false
  }
  // Try to adapt to the input data.
  // We flush if the ratio between matches and literals
  // changes beyound a certain threshold
  nLits = (litCount - matchCount)
  if (nLits <= matchCount) {return false} // whow! so many matches
  return ((nLits * 4) <= matchCount)
}

method storedBlockSize DeflateStream {
  // Compute the length for the current block when stored as is
  return (+ 3
            ((8 - (((bitPosition encoder) + 3) & 7)) & 7)
            32
            ((blockPosition - blockStart) * 8))
}

method updateCrc DeflateStream {
  if (crcPosition <= position) {
    bytesWritten = (((bytesWritten + position) - crcPosition) + 1)
    if (crcType == 'crc32') {
      crc = (updateCrc32 crc crcPosition position collection)
    } (crcType == 'adler32') {
      crc = (updateAdler32 crc crcPosition position collection)
      // print 'crc' (toStringBase16 crc) crcPosition position
    } else {
    }
    crcPosition = (position + 1)
  }
}


method writeFooter DeflateStream {
  // Write footer information if necessary
  crc = (All1 ^ crc)
}

method zLibWriteHeader DeflateStream {
  // Write header information
  nextBitsPut encoder 8 120 // deflate method with 15bit window size
  nextBitsPut encoder 8 94 // checksum; no preset; fast (flevel=1) compression
}

method zLibWriteFooter DeflateStream {
  // Store the Adler32 checksum as the last 4 bytes.
  for ii 4 {
    i = (4 - ii)
    nextBytePut encoder ((crc >> (i * 8)) & 255)
  }
}

defineClass ZipEncoder collection position bitBuffer bitPosition encoded MinMatch MatchLengthCodes NumLiterals BaseLength BaseDistance ExtraLengthBits DistanceCodes ExtraDistanceBits EndBlock

method init ZipEncoder {
  MinMatch = 3
  DistanceCodes = (array 0 1 2 3 4 4 5 5 6 6 6 6 7 7 7 7 8 8 8 8 8 8 8 8 9 9 9 9 9 9 9 9 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 11 11 11 11 11 11 11 11 11 11 11 11 11 11 11 11 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 14 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 0 0 16 17 18 18 19 19 20 20 20 20 21 21 21 21 22 22 22 22 22 22 22 22 23 23 23 23 23 23 23 23 24 24 24 24 24 24 24 24 24 24 24 24 24 24 24 24 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 26 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 27 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 28 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29 29)
  MatchLengthCodes = (array 257 258 259 260 261 262 263 264 265 265 266 266 267 267 268 268 269 269 269 269 270 270 270 270 271 271 271 271 272 272 272 272 273 273 273 273 273 273 273 273 274 274 274 274 274 274 274 274 275 275 275 275 275 275 275 275 276 276 276 276 276 276 276 276 277 277 277 277 277 277 277 277 277 277 277 277 277 277 277 277 278 278 278 278 278 278 278 278 278 278 278 278 278 278 278 278 279 279 279 279 279 279 279 279 279 279 279 279 279 279 279 279 280 280 280 280 280 280 280 280 280 280 280 280 280 280 280 280 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 281 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 282 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 283 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284 284)

  ExtraLengthBits = (array 0 0 0 0 0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 5 5 5 0)

  NumLiterals = 256

  BaseDistance = (array 0 1 2 3 4 6 8 12 16 24 32 48 64 96 128 192 256 384 512 768 1024 1536 2048 3072 4096 6144 8192 12288 16384 24576)
  BaseLength = (array 0 1 2 3 4 5 6 7 8 10 12 14 16 20 24 28 32 40 48 56 64 80 96 112 128 160 192 224 0)
  ExtraDistanceBits = (array 0 0 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13)
  EndBlock = 256
}

method contents ZipEncoder {
  return (contents encoded)
}

method on ZipEncoder data {
  init this

  if ((classOf data) == (class 'BinaryData')) {
    encoded = (dataStream data true)
  } else {
    // assume it is a DataStream
    encoded = data
  }
  collection = (newBinaryData 4096)
  position = 0
  bitPosition = 0
  bitBuffer = 0
  return this
}

method bitPosition ZipEncoder {
  return ((((getField encoded 'position') + position) * 8) + bitPosition)
}

method position ZipEncoder {
  return position
}

method close ZipEncoder {
  flush this
}

method commit ZipEncoder {
  nextPutAll encoded collection 1 position
  position = 0
}

method flush ZipEncoder {
  flushBits this
  commit this
}

method flushBits ZipEncoder {
  while (bitPosition > 0) {
    nextBytePut this (bitBuffer & 255)
    bitBuffer = (bitBuffer >> 8)
    bitPosition = (bitPosition - 8)
  }
  bitPosition = 0
}

method nextBitsPut ZipEncoder nBits value {
  // Store a value of nBits
  bitBuffer = (bitBuffer | (value << bitPosition))
  bitPosition += nBits
  while (bitPosition >= 8) {
    nextBytePut this (bitBuffer & 255)
    bitBuffer = (bitBuffer >> 8)
    bitPosition = (bitPosition - 8)
  }
}

method nextBytePut ZipEncoder value {
  if (position >= (byteCount collection)) {
    return (pastEndPut this value)
  }
  position += 1
  return (byteAtPut collection position value)
}

method pastEndPut ZipEncoder value {
  // Flush the current buffer and store the new object at the beginning
  commit this
  nextBytePut this value
}

method sendBlock ZipEncoder literalStream distanceStream litTree distTree {
  // Send the current block using the encodings from the given literal/length and distance tree
  result = 0
  while (not (atEnd literalStream)) {
//   result = (result + (sendBlockHelper this literalStream distanceStream litTree distTree))
   result = (result + (zipSendBlock this literalStream distanceStream litTree distTree))
    commit this
  }
  nextBitsPut this (bitLengthAt litTree EndBlock) (codeAt litTree EndBlock)
  return result
}

method sendBlockHelper ZipEncoder literalStream distanceStream litTree distTree {
  // Send the current block using the encodings from the given literal/length and distance tree
  sum = 0
  while (and (not (atEnd literalStream)) ((position + 8) < (byteCount collection))) {
    lit = (nextUInt8 literalStream)
    dist = (nextUInt32 distanceStream)
    if (dist == 0) {// lit is a literal
      sum = (sum + 1)
      nextBitsPut this (bitLengthAt litTree lit) (codeAt litTree lit)
    } else { // lit is match length
      sum = ((sum + lit) + MinMatch)
      code = (at MatchLengthCodes (lit + 1))
      nextBitsPut this (bitLengthAt litTree code) (codeAt litTree code)
      extra = (at ExtraLengthBits (code - NumLiterals))
      if (extra != 0) {
        lit = (lit - (at BaseLength (code - NumLiterals)))
        nextBitsPut this extra lit
      }
      dist = (dist - 1)
      if (dist < 256) {
        code = (at DistanceCodes (dist + 1))
      } else {
        code = (at DistanceCodes (257 + (dist >> 7)))
      }
      nextBitsPut this (bitLengthAt distTree code) (codeAt distTree code)
      extra = (at ExtraDistanceBits (code + 1))
      if (extra != 0) {
        dist = (dist - (at BaseDistance (code + 1)))
        nextBitsPut this extra dist
      }
    }
  }
  return sum
}

defineClass ZipEncoderTree bitLengths codes maxCode

method bitLengthAt ZipEncoderTree index {
  return (at bitLengths (index + 1))
}

method bitLengths ZipEncoderTree {
  return bitLengths
}

method maxCode ZipEncoderTree {
  return maxCode
}

method codeAt ZipEncoderTree index {
  return (at codes (index + 1))
}

method setBitLengths ZipEncoderTree blArray codeArray {
  bitLengths = (toUInt32Array blArray)
  codes = (toUInt32Array codeArray)
}

method reverseBits ZipEncoderTree code length {
  // Bit reverse the given code
  result = 0
  bits = code
  for i length {
    bit = (bits & 1)
    result = ((result << 1) | bit)
    bits = (bits >> 1)
  }
  return result
}

method setValuesFrom ZipEncoderTree nodeList {
  newLengths = (new 'UInt32Array' (newBinaryData ((maxCode + 1) * 4)))
  newCodes = (new 'UInt32Array' (newBinaryData ((maxCode + 1) * 4)))
//  newCodes = (newArray (maxCode + 1))
  for i (maxCode + 1) {
    atPut newLengths i (bitLength (at nodeList i))
    atPut newCodes i (code (at nodeList i))
  }
  setBitLengths this newLengths newCodes
}

method buildCodes ZipEncoderTree nodeList blCounts depth {
  // Build the codes for all nodes
  nextCode = (new 'UInt32Array' (newBinaryData ((depth + 1) * 4)))
  code = 0
  for bits depth {
    code = ((code + (at blCounts bits)) << 1)
    atPut nextCode (bits + 1) code
  }
//  assert ((code + (at blCounts (depth + 1))) - 1) ((1 << depth) - 1) 'buildCodes'
  for nn (maxCode + 1) {
    n = (nn - 1)
    node = (at nodeList (n + 1))
    length = (bitLength node)
    if (length != 0) {
      code = (at nextCode (length + 1))
      setCode node (reverseBits this code length)
      atPut nextCode (length + 1) (code + 1)
    }
  }
}

method buildHierarchyFrom ZipEncoderTree aHeap {
  // Build the node hierarchy based on the leafs in aHeap
  while ((count aHeap) > 1) {
    left = (removeFirst aHeap)
    right = (removeFirst aHeap)
    parent = (init (new 'ZipEncoderNode') -1 ((frequency left) + (frequency right))
    	     	    ((max (height left) (height right)) + 1))
    setParent left parent
    setParent right parent
    setLeft parent left
    setRight parent right
    add aHeap parent
  }
  return (removeFirst aHeap)
}

method buildTree ZipEncoderTree nodeList depth {
  // Build either the literal or the distance tree

  heap = (heap ((count nodeList) / 3))

  // Find all nodes with non-zero frequency and add to heap
  maxCode = 0
  for dNode nodeList {
    if ((frequency dNode) != 0) {
      maxCode = (value dNode)
      add heap dNode
    }
  }
  // The pkzip format requires that at least one distance code exists,
  // and that at least one bit should be sent even if there is only one
  // possible code. So to avoid special checks later on we force at least
  // two codes of non zero frequency.
  
  if ((count heap) == 0) {
//    assert maxCode 0
    add heap (at nodeList 1)
    add heap (at nodeList 2)
    maxCode = 1
  }

  if ((count heap) == 1) {
    if ((frequency (at nodeList 1)) == 0) {
      add heap (at nodeList 1)
    } else {
      add heap (at nodeList 2)
    }
    maxCode = (max maxCode 1)
  }
  
  rootNode = (buildHierarchyFrom this heap)
  if ((height rootNode) > depth) {
    rootNode = (rotateToHeight rootNode depth)
    if ((height rootNode) > depth) {
      error 'Cannot encode tree'
    }
  }
  blCounts = (new 'UInt32Array' (newBinaryData (4 * (depth  + 1))))
  encodeBitLength rootNode blCounts this
  buildCodes this nodeList blCounts depth
  setValuesFrom this nodeList
}

method buildTreeFrom ZipEncoderTree frequencies depth {
  // Build the receiver from the given frequency values
  nodeList = (newArray (count frequencies))
  for i (count frequencies) {
    atPut nodeList i (init (new 'ZipEncoderNode') (i - 1) (at frequencies i) 0)
  }
  buildTree this nodeList depth
  return this
}

defineClass ZipEncoderNode value frequency height bitLength code parent left right

method bitLength ZipEncoderNode {
  if (notNil bitLength) {return bitLength}
  return 0
}

method toString ZipEncoderNode {
  return (join 'ZipEncoderNode(value = ' (toString value) ', freq = ' (toString frequency) ', bitLength = ' (toString bitLength) ', code = ' (toString code) ', height = ' (toString height) ')')
}

method code ZipEncoderNode {
  if (notNil code) {return code}
  return 0
}

method setCode ZipEncoderNode aCode {
  code = aCode
}

method computeHeight ZipEncoderNode {
  if (isLeaf this) {
    height = 0
  } else {
    height = ((max (computeHeight left) (computeHeight right)) + 1)
  }
  return height
}

method encodeBitLength ZipEncoderNode blCounts aTree {
  // Note: If bitLength is not nil then the tree must be broken
  if (notNil bitLength) {error 'Huffman tree is broken'}
  if (isNil parent) {
    bitLength = 0
  } else {
    bitLength = ((bitLength parent) + 1)
  }
  
  if (isLeaf this) {
    index = (bitLength + 1)
    atPut blCounts index ((at blCounts index) + 1)
  } else {
    encodeBitLength left blCounts aTree
    encodeBitLength right blCounts aTree
  }
}

method frequency ZipEncoderNode {
  return frequency
}

method setFrequency ZipEncoderNode aValue {
  frequency = aValue
}

method height ZipEncoderNode {
  return height
}

method isLeaf ZipEncoderNode {
  return (isNil left)
}

method leafNodes ZipEncoderNode {
  if (isLeaf this) {
    return (array this)
  } else {
    return (join (leafNodes left) (leafNodes right))
  }
}

method left ZipEncoderNode {
  return left
}

method setLeft ZipEncoderNode aNode {
  setParent aNode this
  left = aNode
}

method right ZipEncoderNode {
  return right
}

method setRight ZipEncoderNode aNode {
  setParent aNode this
  right = aNode
}

method parent ZipEncoderNode {
  return parent
}

method setParent ZipEncoderNode aNode {
  parent = aNode
}

method <= ZipEncoderNode aNode {
  // This method determines the sort ordering in Heap
  if (frequency == (frequency aNode)) {
    return (height <= (height aNode))
  }
  return (frequency < (frequency aNode))
}

method rotateToHeight ZipEncoderNode maxHeight {
  // Rotate the tree to achieve maxHeight depth
  if (height < 4) {return this}
  setLeft this (rotateToHeight left (maxHeight - 1))
  setRight this (rotateToHeight right (maxHeight - 1))

  height = ((max (height left) (height right)) + 1)
  if (height <= maxHeight) {return this}
  if ((abs ((height left) - (height right))) <= 2) {return this}

  if ((height left) < (height right)) {
    if ((height (right right)) >= (height (left right))) {
      newParent = right
      setRight this (left newParent)
      setLeft newParent this
    } else {
      newParent = (left right)
      setLeft right (right newParent)
      setRight newParent right
      setRight this (left newParent)
      setLeft newParent this
    }
  } else {
    if ((height (left left)) >= (height (right left))) {
      newParent = left
      setLeft this (right newParent)
      setRight newParent this
    } else {
      newParent = (right left)
      setRight left (left newParent)
      setLeft newParent left
      setLeft this (right newParent)
      setRight newParent this
    }
  }
  computeHeight parent
  return parent
}

method setBitLength ZipEncoderNode aValue {
  bitLength = aValue
}

method value ZipEncoderNode {
  return value
}

method init ZipEncoderNode v f h {
  value = v
  frequency = f
  height = h
  return this
}
