call (function {
  encoder = (on (new 'ZipEncoder') (newBinaryData 100))

  nextBitsPut encoder 3 5
  assert (bitPosition encoder) 3 'one 3 bits'
  assert (position encoder) 0    'one 3 bits'

  nextBitsPut encoder 3 5
  assert (bitPosition encoder) 6 'two 3 bits'
  assert (position encoder) 0    'two 3 bits'

  nextBitsPut encoder 3 5
  assert (bitPosition encoder) 9 'three 3 bits'
  assert (position encoder) 1    'a full byte'

  assert (byteAt (getField encoder 'collection') 1) 109 '1101101'
  assert (getField encoder 'bitBuffer') 1 '01'

  flush encoder
  data = (getField (getField encoder 'encoded') 'data')
  assert (byteAt data 1) 109 'flush 1101101'
  assert (byteAt data 2) 1 'flush 01'
  assert (byteAt data 3) 0 'flush'
})
