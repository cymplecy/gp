defineClass OMetaTranslator cls level cursors currentList matcherError memo
method match OMetaTranslator input rule {
  cursors = (list 1)
  matcherError = (array 'matcherror')
  currentList = input
  return (call rule this)
}
method apply OMetaTranslator rule {
  shouldMemo = false
  if (notNil memo) {
    if ((argCount) == 2) {
      if ((count cursors) == 1) {
        pos = (at cursors 1)
        m = (at memo pos)
        if (notNil m) {
          v = (at m rule)
          if (notNil v) {
            atPut cursors 1 (at v 2)
            return (at v 1)
          }
        }
        shouldMemo = true
      }
    }
  }
  if ((argCount) == 2) {
    result = (call rule this)
  } else {
    args = (newArray ((argCount) - 1))
    atPut args 1 this
    for i ((argCount) - 2) {
      atPut args (i + 1) (arg (i + 2))
    }
    result = (callWith rule args)
  }
  if (not shouldMemo) {
    return result
  }
  newPos = (at cursors 1)
  if (isNil m) {
    m = (dictionary)
    atPut memo pos m
  }
  atPut m rule (array result newPos)
  return result
}
method exactly OMetaTranslator str {
  if ((at cursors 1) <= (count currentList)) {
    if (str == (at currentList (at cursors 1))) {
      result = (at currentList (at cursors 1))
      atPut cursors 1 (+ (at cursors 1) 1)
      return result
    }
  }
  return matcherError
}
method seq OMetaTranslator str {
  s = (letters str)
  origCursor = (at cursors 1)
  for i (count s) {
    if ((at cursors 1) <= (count currentList)) {
      if ((at s i) == (at currentList (at cursors 1))) {
        atPut cursors 1 (+ (at cursors 1) 1)
      } else {
        atPut cursors 1 origCursor
        return matcherError
      }
    } else {
      atPut cursors 1 origCursor
      return matcherError
    }
  }
  return str
}
method end OMetaTranslator {
  if ((at cursors 1) > (count currentList)) {
    return true
  }
  return matcherError
}
method token OMetaTranslator str {
  origCursor = (at cursors 1)
  spaces this
  result = (seq this str)
  if (result === matcherError) {
    atPut cursors 1 origCursor
  }
  return result
}
method spaces OMetaTranslator {
  while true {
    if ((at cursors 1) <= (count currentList)) {
       if (isWhiteSpace (at currentList (at cursors 1))) {
         atPut cursors 1 (+ (at cursors 1) 1)
       } else {
         return nil
       }
    } else {
       return nil
    }
  }
  return nil
}
method anything OMetaTranslator {
  if ((at cursors 1) <= (count currentList)) {
    result = (at currentList (at cursors 1))
    atPut cursors 1 (+ (at cursors 1) 1)
    return result
  }
  return matcherError
}
method empty OMetaTranslator {
  return true
}
method useMemo OMetaTranslator {
  memo = (dictionary)
}

method trans OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCurrentList2 = currentList
origCursor2 = (at cursors 1)
done2 = false
result2 = nil
if ((count currentList) < origCursor2) {
result2 = matcherError
done2 = true
}
if (not (result2 === matcherError)) {
c2 = (at currentList origCursor2)
if (and ((classOf c2) != (class 'Array')) ((classOf c2) != (class 'List'))) {
result2 = matcherError
done2 = true
}
}
if (not (result2 === matcherError)) {
currentList = c2
addFirst cursors 1
origCursor3 = (at cursors 1)
result3 = nil
if (not (result3 === matcherError)) {
origCursor4 = (at cursors 1)
result5 = (apply this 'anything')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
} else {
sel = result4
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
if (not (result3 === matcherError)) {
origCursor4 = (at cursors 1)
result5 = (call sel this)
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
} else {
v = result4
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
result2 = result3
if (not (result2 === matcherError)) {
if ((at cursors 1) > (count currentList)) {
result2 = currentList
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 (origCursor2 + 1)
done2 = true
}
}
}
if (not done2) {
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 origCursor2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = v
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method action OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
v = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 'result' d ' = ' v (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method predAction OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
v = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                            'result' d ' = ' v (cr this)
                            'if (not result' d ') {' (cr this)
                            'result' d ' = matcherError' (cr this)
                            '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method and OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (array 'origCursor' d ' = (at cursors 1)' (cr this)
                      'result' d ' = nil' (cr this))
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
init = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
origCursor5 = (at cursors 1)
result6 = (apply this 'trans')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
} else {
r = result5
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (array
                        'if (not (result' d ' === matcherError)) {' (cr this)
                        r
                        'result' d ' = result' n (cr this)
                        'if (result' d ' === matcherError) {' (cr this)
                        'atPut cursors 1 origCursor' d (cr this)
                        '}' (cr this)
                        '}' (cr this))
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
rs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array init (toArray rs))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method or OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (array
                        'origCursor' d ' = (at cursors 1)' (cr this)
                        'done' d ' = false' (cr this)
                        'result' d ' = nil' (cr this))
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
init = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
origCursor5 = (at cursors 1)
result6 = (apply this 'trans')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
} else {
r = result5
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (array
                        'if (not done' d ') {' (cr this)
                        r
                        'result' d ' = result' n (cr this)
                        'if (not (result' d ' === matcherError)) {' (cr this)
                        'done' d ' = true' (cr this)
                        '} else {' (cr this)
                        'atPut cursors 1 origCursor' d (cr this)
                        '}' (cr this)
                        '}' (cr this))
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
rs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (array
                        'if (not done' d ') {' (cr this)
                        'result' d ' = matcherError' (cr this)
                        '}' (cr this))
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
after = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array init (toArray rs) after)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method option OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'at cursors 1 origCursor' d (cr this)
                         'result' d ' = nil' (cr this)
                         '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method not OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'atPut cursors 1 origCursor' d (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'result' d ' = true' (cr this)
                         '} else {' (cr this)
                         'result' d ' = matcherError' (cr this)
                         '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method many0 OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                          'result' d ' = (list)' (cr this)
                          'val' d ' = nil' (cr this)
                          'while (not (val' d ' === matcherError)) {' (cr this)
                          r
                          'val' d ' = result' n (cr this)
                          'if (not (val' d ' === matcherError)) {' (cr this)
                          'add result' d ' val' d (cr this)
                        '}' (cr this)
                        '}' (cr this)
                        )
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method many1 OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         'result' d ' = (list)' (cr this)
                         'val' d ' = nil' (cr this)
                         'first' d ' = true' (cr this)
                         'while (not (val' d ' === matcherError)) {' (cr this)
                        r
                        'val' d ' = result' n (cr this)
                        'if (not (val' d ' === matcherError)) {' (cr this)
                        'add result' d ' val' d (cr this)
                        '} else {' (cr this)
                        'if first' d ' {' (cr this)
                        'atPut cursors 1 origCursor' d (cr this)
                        'result' d ' = matcherError' (cr this)
                        '}' (cr this)
                        '}' (cr this)
                        'first' d ' = false' (cr this)
                        '}' (cr this)
                        )
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method lookahead OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'atPut cursors 1 origCursor' d (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method consume OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'at cursors 1 origCursor' d (cr this)
                         '} else {' (cr this)
                         'result' d ' = (toArray (copyList currentList ((at cursors 1) - origCursor' d ') origCursor' d '))' (cr this)
                         '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method bind OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
v = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (result' d ' === matcherError) {' (cr this)
                         'atPut cursors 1 origCursor' d (cr this)
                         '} else {' (cr this)
                         v ' = result' d (cr this)
                         '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method form OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (nextLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'origCurrentList' d ' = currentList' (cr this)
                         'origCursor' d ' = (at cursors 1)' (cr this)
                         'done' d ' = false' (cr this)
                         'result' d ' = nil' (cr this)
                         'if ((count currentList) < origCursor' d ') {' (cr this)
                         'result' d ' = matcherError' (cr this)
                         'done' d ' = true' (cr this)
                         '}' (cr this)
                         'if (not (result' d ' === matcherError)) {' (cr this)
                         'c' d ' = (at currentList origCursor' d ')' (cr this)
                         'if (and ((classOf c' d ') != (class ''Array'')) ((classOf c' d ') != (class ''List''))) {' (cr this)
                         'result' d ' = matcherError' (cr this)
                         'done' d ' = true' (cr this)
                         '}' (cr this)
                         '}' (cr this)
                         'if (not (result' d ' === matcherError)) {' (cr this)
                         'currentList = c' d (cr this)
                         'addFirst cursors 1' (cr this)
                         r
                         'result' d ' = result' n (cr this)
                         'if (not (result' d ' === matcherError)) {' (cr this)
                         'if ((at cursors 1) > (count currentList)) {' (cr this)
                         'result' d ' = currentList' (cr this)
                         'currentList = origCurrentList' d (cr this)
                         'removeFirst cursors' (cr this)
                         'atPut cursors 1 (origCursor' d ' + 1)' (cr this)
                         'done' d ' = true' (cr this)
                         '}' (cr this)
                         '}' (cr this)
                         '}' (cr this)
                         'if (not done' d ') {' (cr this)
                         'currentList = origCurrentList' d (cr this)
                         'removeFirst cursors' (cr this)
                         'atPut cursors 1 origCursor' d (cr this)
                         '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method app OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (pushLevel this)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
d = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
result4 = (apply this 'anything')
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
as = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (popLevel this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (appFunc this n as d)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method rule OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCurrentList2 = currentList
origCursor2 = (at cursors 1)
done2 = false
result2 = nil
if ((count currentList) < origCursor2) {
result2 = matcherError
done2 = true
}
if (not (result2 === matcherError)) {
c2 = (at currentList origCursor2)
if (and ((classOf c2) != (class 'Array')) ((classOf c2) != (class 'List'))) {
result2 = matcherError
done2 = true
}
}
if (not (result2 === matcherError)) {
currentList = c2
addFirst cursors 1
origCursor3 = (at cursors 1)
result3 = nil
if (not (result3 === matcherError)) {
origCursor4 = (at cursors 1)
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
result6 = (apply this 'anything')
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
} else {
as = result4
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
result2 = result3
if (not (result2 === matcherError)) {
if ((at cursors 1) > (count currentList)) {
result2 = currentList
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 (origCursor2 + 1)
done2 = true
}
}
}
if (not done2) {
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 origCursor2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCurrentList2 = currentList
origCursor2 = (at cursors 1)
done2 = false
result2 = nil
if ((count currentList) < origCursor2) {
result2 = matcherError
done2 = true
}
if (not (result2 === matcherError)) {
c2 = (at currentList origCursor2)
if (and ((classOf c2) != (class 'Array')) ((classOf c2) != (class 'List'))) {
result2 = matcherError
done2 = true
}
}
if (not (result2 === matcherError)) {
currentList = c2
addFirst cursors 1
origCursor3 = (at cursors 1)
result3 = nil
if (not (result3 === matcherError)) {
origCursor4 = (at cursors 1)
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
result6 = (apply this 'anything')
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
} else {
ts = result4
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
result2 = result3
if (not (result2 === matcherError)) {
if ((at cursors 1) > (count currentList)) {
result2 = currentList
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 (origCursor2 + 1)
done2 = true
}
}
}
if (not done2) {
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 origCursor2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'trans')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'method ' n ' ' (cls this) (map (function a {return (array ' ' a)}) as) ' {' (cr this)
                         r
                         'result1 = result2' (cr this)
                         'return result1' (cr this)
                         '}' (cr this))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method grammar OMetaTranslator {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
cls = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCurrentList2 = currentList
origCursor2 = (at cursors 1)
done2 = false
result2 = nil
if ((count currentList) < origCursor2) {
result2 = matcherError
done2 = true
}
if (not (result2 === matcherError)) {
c2 = (at currentList origCursor2)
if (and ((classOf c2) != (class 'Array')) ((classOf c2) != (class 'List'))) {
result2 = matcherError
done2 = true
}
}
if (not (result2 === matcherError)) {
currentList = c2
addFirst cursors 1
origCursor3 = (at cursors 1)
result3 = nil
if (not (result3 === matcherError)) {
origCursor4 = (at cursors 1)
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
result6 = (apply this 'anything')
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
} else {
insts = result4
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
result2 = result3
if (not (result2 === matcherError)) {
if ((at cursors 1) > (count currentList)) {
result2 = currentList
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 (origCursor2 + 1)
done2 = true
}
}
}
if (not done2) {
currentList = origCurrentList2
removeFirst cursors
atPut cursors 1 origCursor2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
rest = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
result4 = (apply this 'trans')
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
rs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                         'defineClass ' cls (map (function i {return (array ' ' i)}) insts) ' cursors currentList matcherError memo' (cr this)
                         'method match ' cls ' input rule {' (cr this)
                         '  cursors = (list 1)' (cr this)
                         '  matcherError = (array ''matcherror'')' (cr this)
                         '  currentList = input' (cr this)
                         '  return (call rule this)' (cr this)
                         '}' (cr this)

			 'method apply ' cls ' rule {' (cr this)
			 '  shouldMemo = false' (cr this)
			 '  if (notNil memo) {' (cr this)
			 '    if ((argCount) == 2) {' (cr this)
			 '      if ((count cursors) == 1) {' (cr this)
			 '        pos = (at cursors 1)' (cr this)
			 '        m = (at memo pos)' (cr this)
			 '        if (notNil m) {' (cr this)
			 '          v = (at m rule)' (cr this)
			 '          if (notNil v) {' (cr this)
			 '            atPut cursors 1 (at v 2)' (cr this)
			 '            return (at v 1)' (cr this)
			 '          }' (cr this)
			 '        }' (cr this)
			 '        shouldMemo = true' (cr this)
			 '      }' (cr this)
			 '    }' (cr this)
			 '  }' (cr this)
			 '  if ((argCount) == 2) {' (cr this)
			 '    result = (call rule this)' (cr this)
			 '  } else {' (cr this)
			 '    args = (newArray ((argCount) - 1))' (cr this)
			 '    atPut args 1 this' (cr this)
			 '    for i ((argCount) - 2) {' (cr this)
			 '      atPut args (i + 1) (arg (i + 2))' (cr this)
			 '    }' (cr this)
			 '    result = (callWith rule args)' (cr this)
			 '  }' (cr this)
			 '  if (not shouldMemo) {' (cr this)
			 '    return result' (cr this)
			 '  }' (cr this)
			 '  newPos = (at cursors 1)' (cr this)
			 '  if (isNil m) {' (cr this)
			 '    m = (dictionary)' (cr this)
			 '    atPut memo pos m' (cr this)
			 '  }' (cr this)
			 '  atPut m rule (array result newPos)' (cr this)
			 '  return result' (cr this)
			 '}' (cr this)
                         'method exactly ' cls ' str {' (cr this)
                         '  if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '    if (str == (at currentList (at cursors 1))) {' (cr this)
                         '      result = (at currentList (at cursors 1))' (cr this)
                         '      atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '      return result' (cr this)
                         '    }' (cr this)
                         '  }' (cr this)
                         '  return matcherError' (cr this)
                         '}' (cr this)
                         'method seq ' cls ' str {' (cr this)
                         '  s = (letters str)' (cr this)
                         '  origCursor = (at cursors 1)' (cr this)
                         '  for i (count s) {' (cr this)
                         '    if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '      if ((at s i) == (at currentList (at cursors 1))) {' (cr this)
                         '        atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '      } else {' (cr this)
                         '        atPut cursors 1 origCursor' (cr this)
                         '        return matcherError' (cr this)
                         '      }' (cr this)
                         '    } else {' (cr this)
                         '      atPut cursors 1 origCursor' (cr this)
                         '      return matcherError' (cr this)
                         '    }' (cr this)
                         '  }' (cr this)
                         '  return str' (cr this)
                         '}' (cr this)
                         'method end ' cls ' {' (cr this)
                         '  if ((at cursors 1) > (count currentList)) {' (cr this)
                         '    return true' (cr this)
                         '  }' (cr this)
                         '  return matcherError' (cr this)
                         '}' (cr this)
                         'method token ' cls ' str {' (cr this)
                         '  origCursor = (at cursors 1)' (cr this)
                         '  spaces this' (cr this)
                         '  result = (seq this str)' (cr this)
                         '  if (result === matcherError) {' (cr this)
                         '    atPut cursors 1 origCursor' (cr this)
                         '  }' (cr this)
                         '  return result' (cr this)
                         '}' (cr this)
                         'method spaces ' cls ' {' (cr this)
                         '  while true {' (cr this)
                         '    if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '       if (isWhiteSpace (at currentList (at cursors 1))) {' (cr this)
                         '         atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '       } else {' (cr this)
                         '         return nil' (cr this)
                         '       }' (cr this)
                         '    } else {' (cr this)
                         '       return nil' (cr this)
                         '    }' (cr this)
                         '  }' (cr this)
                         '  return nil' (cr this)
                         '}' (cr this)
                         'method anything ' cls ' {' (cr this)
                         '  if ((at cursors 1) <= (count currentList)) {' (cr this)
                         '    result = (at currentList (at cursors 1))' (cr this)
                         '    atPut cursors 1 (+ (at cursors 1) 1)' (cr this)
                         '    return result' (cr this)
                         '  }' (cr this)
                         '  return matcherError' (cr this)
                         '}' (cr this)
                         'method empty ' cls ' {' (cr this)
                         '  return true' (cr this)
                         '}' (cr this)
 			 'method useMemo ' cls ' {' (cr this)
			 '  memo = (dictionary)' (cr this)
			 '}' (cr this)

                         (map (function r {return (array (stringFromCodePoints (array 10)) r)}) rs) rest)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}


method appFunc OMetaTranslator n as d {
  return (array 'result' d ' = (apply this ' (printString n) (map (function a {(array ' ' (printString a))}) as) ')' (cr this))
}

method cr OMetaTranslator {
  return (stringFromCodePoints (array 10))
}

method cls OMetaTranslator {
  if (cls != nil) {
    return cls
  } else {
    return 'Unknown'
  }
}

method pushLevel OMetaTranslator {
  level += 1
  return (toString level)
}

method nextLevel OMetaTranslator {
  return (toString (level + 1))
}

method popLevel OMetaTranslator {
  level = (level - 1)
  return (toString level)
}

