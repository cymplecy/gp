rule OMetaParser temps {
        args            = '(' gpExpressionString:g1 (spaces ',' gpExpressionString)*:gs ")"
                            -> {(join (array g1) gs)}
                        | ~'(' -> {(array)},
        letter          = anything:x ?{(isLetter x)} -> {x},
        digit           = anything:x ?{(isDigit x)}  -> {x},
        name            = spaces < letter (letter | digit)* >:cs
                            -> {(joinStringArray cs)},
        optIter         = "*" -> {'many0'} | "+" -> {'many1'} | "?" ~'{' -> {'option'},
        application     = name:r args:as -> {(join (array 'app' r) as)},
        semanticAction  = "->"? "{" gpExpressionString:e "}" -> {(array 'action' e)},
        semanticPredicate = "?{" gpExpressionString:e "}" -> {(array 'predAction' e)},
        stringLiteral   =  "'" ((&('''' '''') anything anything) | ~'''' anything)*:xs ''''
                            -> {(array 'app' 'seq' (joinStringArray (toArray xs)))},
        stringAtom      =  "#'" ((&('''' '''') anything anything) | ~'''' anything)*:xs ''''
                            -> {(array 'app' 'exactly' (joinStringArray (toArray xs)))},
        tokenSugar      = spaces '"' < (~'"' anything)* >:xs '"'
                            -> {(array 'app' 'token' (joinStringArray (toArray xs)))},
        expr            = expr4:e1 (spaces '|' expr4)+:es
                            -> {(join (array 'or' e1) es)}
                        | expr4,
        expr1           = application
                        | semanticAction
                        | semanticPredicate
                        | tokenSugar
                        | stringAtom
                        | stringLiteral
                        | "[" expr:e "]" -> {(array 'form' e)}
                        | "<" expr:e ">" -> {(array 'consume' e)}
                        | "(" expr:e ")" -> {e},
        expr2           = "~" expr2:x -> {(array 'not' x)}
                        | "&" expr2:x -> {(array 'lookahead' x)}
                        | expr1,
        expr3           = (expr2:x optIter:o -> {(array o x)} | expr2):x
                             (":" name:n {(addTemp this n)}
                                -> {(array 'bind' n x)}
                             | empty -> {x}),
        expr4           = expr3*:xs -> {(join (array 'and') xs)},
        gpExpression      = (spaces '''' ((&('''' '''') anything anything) | ~'''' anything)* ''''
                          | '(' gpExpression ')'
                          | '{' gpExpression '}'
                          | anything:c ?{(and (c != '}') (c != ')'))})*,
        gpExpressionString = spaces < gpExpression >:s
                            -> {(joinStringArray s)},
        rule            = name:n (":" name)*:as "=" {(resetTemps this)} expr:e
                            -> {(array
                                 'rule' n
                                 (toArray as)
                                 (keys (getField this 'temps'))
                                 e)},
        grammar         = "rule" name:cls name*:vars "{"
                              rule:r1 ("," rule)*:rs "}" < anything* >:rest
                            -> {(join (array 'grammar'
                                             cls
                                             (toArray vars)
                                             (joinStringArray rest)
                                             r1)
                                       rs)}
}

method addTemp OMetaParser t {
  if (temps === nil) {
    temps = (dictionary)
  }
  add temps t
}

method resetTemps OMetaParser {
  temps = (dictionary)
}
