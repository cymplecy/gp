defineClass GeziraVectorPen penX penY heading bitmap owner path gezira clip rasterize fill

to newGeziraVectorPen bitmap owningMorph { return (intialize (new 'GeziraVectorPen') bitmap owningMorph) }

method intialize GeziraVectorPen aBitmap aMorph {
  penX = 100
  penY = 100
  heading = 0
  owner = aMorph
  path = (list)
  gezira = (global 'geziraModule')
  if (isNil gezira) {
	initializeGezira 2
	gezira = (loadModule 'Gezira')
	setGlobal 'geziraModule' gezira
  }
  rasterize = (init (new (at gezira 'Rasterize')))
  setBitmap this aBitmap
  return this
}

method bitmap GeziraVectorPen { return bitmap }

method setBitmap GeziraVectorPen aBitmap {
  bitmap = aBitmap
  clip = (init (new (at gezira 'ClipBeziers')))
  setMinx clip 0
  setMiny clip 0
  setMaxx clip (width bitmap)
  setMaxy clip (height bitmap)
  fill = (init (new (at gezira 'CompositeUniformColorOverImageARGB32')))
  setImage fill (allocateGeziraImage bitmap)
}

method clear GeziraVectorPen color {
  if (isNil color) { color = (gray 255) }
  fill bitmap color
  if (notNil owner) { costumeChanged owner }
}

method beginPath GeziraVectorPen x y {
  if (isNil x) { x = 100 }
  if (isNil y) { y = 100 }
  path = (list)
  penX = x
  penY = y
  heading = 0
}

method forward GeziraVectorPen dist curvature {
  startX = penX
  startY = penY
  penX += (dist * (cos heading))
  penY += (dist * (sin heading))
  addSegment this startX startY penX penY curvature
}

method setHeading GeziraVectorPen degrees { heading = degrees }

method turn GeziraVectorPen degrees {
  heading = ((heading + degrees) % 360)
}

method stroke GeziraVectorPen color width joint cap {
  if (isNil color) { color = (color) }
  if (isNil width) { width = 1 }
  if (isNil joint) { joint = -1 }
  if (isNil cap) { cap = -1 }

  stroke = (init (new (at gezira 'StrokeBezierPath')))
  setW stroke (toFloat width)
  setL stroke (toFloat joint)
  setC stroke (toFloat cap)

  setCR fill ((red color) / 255.0)
  setCG fill ((green color) / 255.0)
  setCB fill ((blue color) / 255.0)
  setCA fill ((alpha color) / 255.0)

  pipe = (geziraPipeline stroke clip rasterize fill)
  geziraFeed pipe (getField (toFloat32Array path) 'data')
  geziraSync
  if (notNil owner) { costumeChanged owner }
}

method fill GeziraVectorPen color {
  if (isNil color) { color = (color) }

  closePath this

  setCR fill ((red color) / 255.0)
  setCG fill ((green color) / 255.0)
  setCB fill ((blue color) / 255.0)
  setCA fill ((alpha color) / 255.0)

  pipe = (geziraPipeline clip rasterize fill)
  geziraFeed pipe (getField (toFloat32Array path) 'data')
  geziraSync
  if (notNil owner) { costumeChanged owner }
}

method addSegment GeziraVectorPen x1 y1 x2 y2 curvature {
  if (isNil curvature) { curvature = 0 }
  if (curvature == 0) {
	cx = x1
	cy = y1
  } else {
	// control point is on a line perpendicular to the segment
	// at it's midpoint, scaled by the curvature
	midpointX = ((x1 + x2) / 2)
	midpointY = ((y1 + y2) / 2)
	cx = (midpointX + (curvature * (y2 - y1)))
	cy = (midpointY + (curvature * (x1 - x2)))
  }
  addAll path (array x1 y1 cx cy x2 y2)
}

method closePath GeziraVectorPen {
  n = (count path)
  if (n < 2) { return }
  firstX = (at path 1)
  firstY = (at path 2)
  lastX = (at path (n - 1))
  lastY = (at path n)
  if (or (lastX != firstX) (lastY != firstY)) {
	addSegment this lastX lastY firstX firstY
  }
}

// xxx testing/debugging

// openWindow; p = (newGeziraVectorPen (newBitmap 500 500))
// clear p; drawStar p; show p

method show GeziraVectorPen { // xxx for debugging
  clearBuffer
  drawBitmap nil bitmap
  flipBuffer
}

method drawBox GeziraVectorPen {
  beginPath this 20 20
  setHeading this 0
  repeat 4 {
	forward this 100 0.1
	turn this 90
  }
  fill this (color 200 0 0)
  stroke this (gray 0) 3
}

method drawStar GeziraVectorPen {
  beginPath this 20 100
  setHeading this 0
  repeat 5 {
	forward this 400
	turn this (180 - 36)
  }
  fill this (color 200 0 0)
  stroke this (gray 0) 3
}

method drawPentagon GeziraVectorPen {
  beginPath this 100 20
  setHeading this 5
  repeat 5 {
	forward this 200
	turn this 72
	print penX penY
  }
  fill this (color 200 0 0)
  stroke this (gray 0) 3
}
