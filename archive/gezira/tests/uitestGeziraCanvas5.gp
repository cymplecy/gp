call (function {

gezira = (loadModule 'GeziraCanvas')
canvas = (init (new (at gezira 'GeziraCanvas')))
bitmap = (newBitmap 500 500)
fill bitmap (color 0 0 0 0)
setTarget canvas bitmap

box = (call (at gezira 'rectPath') 0 0 150 150 0)
circle = (call (at gezira 'arcPath') 0 360 150 125 50 50 0)

tr = (beIdentity (new (at gezira 'Matrix')))
translateBy tr 120 100
rotateBy tr 45
transformBy canvas tr

fill = (init (new (at gezira 'LinearGradientFill')))

setStart fill 200 200
setEnd fill 300 300

addStop fill (color 255 0 0) 0
addStop fill (color 100 0 0 100) 0.2
addStop fill (color 0 255 0) 1

setFill canvas fill
drawPath canvas box

fill = (init (new (at gezira 'UniformColorFill')))
setColor fill (color 0 255 0)
setFill canvas fill
drawPath canvas circle

sync canvas
// bitmap now has colored box and circle

orig = (newBitmap 500 500)
fill orig (color 255 255 255 255)
canvas = (init (new (at gezira 'GeziraCanvas')))
setTarget canvas orig

tr = (beIdentity (new (at gezira 'Matrix')))
translateBy tr 250 0
transformBy canvas tr

imageFill = (init (new (at gezira 'ImageFill')))
setBitmap imageFill bitmap
setFill canvas imageFill

drawPath canvas (call (at gezira 'rectPath') 0 0 250 500 0)

sync canvas
// orig has a copy from bitmap but translated.

canvas = (init (new (at gezira 'GeziraCanvas')))
setTarget canvas bitmap

gray = (newBitmap 500 500)
fill gray (color 200 200 200)

compositor = (init (new (at gezira 'CompositionFill')))
setBitmap compositor gray
setRule compositor 'CompositeSrcIn'

setFill canvas compositor

background = (call (at gezira 'rectPath') 0 0 500 500 0)
drawPath canvas background
sync canvas
// bitmap's non transparent pixels are replaced with gray

canvas = (init (new (at gezira 'GeziraCanvas')))
setTarget canvas orig

imageFill = (init (new (at gezira 'ImageFill')))
setBitmap imageFill bitmap
setFill canvas imageFill
drawPath canvas (call (at gezira 'rectPath') 0 0 500 500 0)
sync canvas

openWindow

drawBitmap nil orig
flipBuffer
sleep 2000

geziraShutDown
})
