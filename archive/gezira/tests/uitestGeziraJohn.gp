call (function {

gezira = (loadModule 'GeziraCanvas')
canvas = (init (new (at gezira 'GeziraCanvas')))
bitmap = (newBitmap 500 500)
fill bitmap (color 255 255 255)

setTarget canvas bitmap
setFlip canvas false

openWindow

boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

//topBox = (init (new Box))
//setExtent topBox 500 500
//
//repeat 100 {
//  aBox = (init (new Box))
//  setPosition aBox (rand 500) (rand 500)
//  setExtent aBox 100 100
////  setScale aBox (rand 0.5 1.5)
//  addPart topBox aBox
//}

//a = (call (at boxModule 'openBoxAsWindow') topBox)
//startStepping a


t = (newTimer)

box = (call (at gezira 'rectPath') 0 0 100 100 10)

tr = (beIdentity (new (at gezira 'Matrix')))
translateBy tr 200 200
transformBy canvas tr
tr = (beIdentity (new (at gezira 'Matrix')))

translateBy tr -10 -10
print tr

  fill = (init (new (at gezira 'UniformColorFill')))
  color = (color (rand 0 255) (rand 0 255) (rand 0 255) (255 * 0.6))
  setColor fill color
  setFill canvas fill

repeat 100 {
//  fill = (init (new (at gezira 'UniformColorFill')))
//  color = (color (rand 0 255) (rand 0 255) (rand 0 255) (255 * 0.6))
//  setColor fill color
//  setFill canvas fill
//  tr = (beIdentity (new (at gezira 'Matrix')))
//  translateBy tr (rand -10 10) (rand -10 10)
//print tr
//  transformBy canvas tr
  drawPath canvas box
}
sync canvas

print 'Gezira Rects:' (usecs t)

drawBitmap nil bitmap
flipBuffer
sleep 2000

//t = (usecsToRun 'drawOn' topBox canvas)
//print t 'usecs'

drawBitmap nil bitmap
flipBuffer
sleep 2000

clearBuffer
t = (newTimer)
repeat 100 {
  fillRect nil (setAlpha (randomColor) 200) (rand 500) (rand 500) 100 100 1
//  fillRect nil (randomColor) (rand 500) (rand 500) 100 100
}
print (usecs t)

clearBuffer
pen = (newPen bitmap)
setAlpha pen 0.7

t = (newTimer)
repeat 100 {
  setColor pen (randomColor)
  fillRoundedRect pen (rect (rand 500) (rand 500) 100 100) 10
}
print (usecs t)


drawBitmap nil bitmap
flipBuffer
sleep 2000

geziraShutDown
})
