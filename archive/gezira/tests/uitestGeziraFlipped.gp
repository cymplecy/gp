call (function {

  star = (float32Array
    250.00000 150.00000 237.65650 183.01064 225.31301 216.02128
    225.31301 216.02128 190.10368 217.55979 154.89434 219.09830
    154.89434 219.09830 182.47498 241.03850 210.05562 262.97871
    210.05562 262.97871 200.63855 296.94020 191.22147 330.90169
    191.22147 330.90169 220.61073 311.45084 250.00000 292.00000
    250.00000 292.00000 279.38926 311.45084 308.77852 330.90169
    308.77852 330.90169 299.36144 296.94020 289.94437 262.97871
    289.94437 262.97871 317.52501 241.03850 345.10565 219.09830
    345.10565 219.09830 309.89631 217.55979 274.68698 216.02128
    274.68698 216.02128 262.34349 183.01064 250.00000 150.00000
)

gezira = (loadModule 'Gezira')
bitmap = (newBitmap 500 500)

call (at gezira 'initGezira')

g = (new (at gezira 'Gate'))
f = (new (at gezira 'FutureGate'))

t = (init (new (at gezira 'TransformBeziers')))
setM11 t 1.0
setM22 t 1.0

c = (init (new (at gezira 'ClipBeziers')))
setMinx c 0
setMiny c 0
setMaxx c 500
setMaxy c 500

r = (init (new (at gezira 'Rasterize')))

u = (init (new (at gezira 'CompositeUniformColorOverImageARGB32Flipped')))
setCA u 0.5
img = (allocateGeziraImage bitmap)
setImage u img

openWindow
for i 200 {

  fill bitmap (color 255 255 255)

  setM31 t (i / 2.0)
  setM32 t (i / 2.0)

  setCR u 1.0
  setCG u 0.0
  setCB u 1.0

  pipe = (geziraPipeline t c r u)
  geziraFeed pipe (getField star 'data')

  setM31 t (0.0 - (i / 2.0))
  setM32 t (0.0 - (i / 2.0))

  setCR u 0.0
  setCG u 1.0
  setCB u 0.0

//  setField g 1 (getField f 1)

//  geziraFeed f (newBinaryData 0)

  pipe = (geziraPipeline t c r u)
  geziraFeed pipe (getField star 'data')

  geziraSync

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer
}

geziraShutDown

})
