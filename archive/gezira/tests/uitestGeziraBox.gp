call (function {
gezira = (loadModule 'GeziraCanvas')
canvas = (init (new (at gezira 'GeziraCanvas')))
bitmap = (newBitmap 500 500)
fill bitmap (color 255 255 255)

setTarget canvas bitmap
setFlip canvas true

boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

aBox = (init (new Box))

setPosition aBox 100 100
setExtent aBox 200 200
setScale aBox 1.5

bBox = (init (new Box))
setPosition bBox 100 180
addPart aBox bBox

cBox = (init (new Box))
setPosition cBox 170 100
setRotation cBox 15
addPart aBox cBox

dBox = (init (new Box))
setPosition dBox -180 0
setScale dBox 2.0
addPart cBox dBox

checkMarkFill = (init (new (at boxModule 'Fill')))
setFill checkMarkFill (call (at boxModule 'uniformColorFill') (color 255 255 255))
setPath checkMarkFill (load 'icons/checkMark.gp')

shape = (init (new (at boxModule 'Shape')))

fillElem1 = (init (new (at boxModule 'Fill')))
fill1 = (call (at boxModule 'linearGradientFill') 0 0 28 28)
addStop fill1 (color 0 2 4) 0
addStop fill1 (color 0 67 121) 1
setPath fillElem1 (call (at gezira 'rectPath') 0 0 28 28 5)
setFill fillElem1 fill1

fillElem2 = (init (new (at boxModule 'Fill')))
fill2 = (call (at boxModule 'radialGradientFill') 10 -10 30)
addStop fill2 (color 176 220 163) 0
addStop fill2 (color 97 186 70) 1
setPath fillElem2 (call (at gezira 'rectPath') 3 3 22 22 3)
setFill fillElem2 fill2

shape = (init (new (at boxModule 'Shape')))
setElements shape (array fillElem1 fillElem2 checkMarkFill)
setShape dBox shape

topBox = (init (new Box))
setExtent topBox 500 500

topFill = (call (at boxModule 'linearGradientFill') 0 0 0 500)

addStop topFill (color 208 208 208) 0
addStop topFill (color 240 240 240) 1

setFill (getField topBox 'shape') topFill

addPart topBox aBox

drawOn topBox canvas

openWindow

drawBitmap nil bitmap
flipBuffer
sleep 2000

geziraShutDown
})
