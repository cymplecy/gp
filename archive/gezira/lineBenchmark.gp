to geziraBench {
  gezira = (loadModule 'GeziraCanvas')

  canvas = (init (new (at gezira 'GeziraCanvas')))
  bitmap = (newBitmap 520 520)
  fill bitmap (color 255 255 255)
  setTarget canvas bitmap

  setStroke canvas (call (at gezira 'stroke'))
  fill = (init (new (at gezira 'UniformColorFill')))
  setColor fill (color)
  setFill canvas fill
  start = (msecsSinceStart)
  for i 100 {
    startY = ((5 * i) + 10)
    endY = ((500 - (5 * i)) + 10)
    drawPath canvas (call (at gezira 'linePath') 10 startY 510 endY)
  }
  sync canvas
  msecs = ((msecsSinceStart) - start)
  print msecs 'msecs'
  return bitmap
}

to penBench antialiasing {
  if (isNil antialiasing) { antialiasing = false }
  bm = (newBitmap 520 520)
  pen = (newPen bm)
  start = (msecsSinceStart)
  for i 100 {
    startY = ((5 * i) + 10)
    endY = ((500 - (5 * i)) + 10)
    goto pen 10 startY false
    down pen
    goto pen 510 endY antialiasing
    up pen
  }
  msecs = ((msecsSinceStart) - start)
  print msecs 'msecs'
  return bm
}

to rotateTest {
Matrix = (at (loadModule 'GeziraMatrix2x3') 'Matrix')

gezira = (loadModule 'Gezira')
bitmap = (newBitmap 500 500)
img = (allocateGeziraImage bitmap)


box = (float32Array 0 0 0 0 200 0
                    200 0 200 0 200 200
                    200 200 200 200 0 200
                    0 200 0 200 0 0)

call (at gezira 'initGezira')

trans = (beIdentity (init (new Matrix)))
setRotation trans 30
setTranslation trans 100 100

t = (init (new (at gezira 'TransformBeziers')))
setField t 'vM' (data trans)

c = (init (new (at gezira 'ClipBeziers')))
setMinx c 0
setMiny c 0
setMaxx c 500
setMaxy c 500

r = (init (new (at gezira 'Rasterize')))
a = (init (new (at gezira 'ApplyTexture')))
comp = (init (new (at gezira 'CompositeTextures')))

gp = (readFrom (new 'PNGReader') (readFile 'tests/gp.png' true))
inv = (init (new (at gezira 'TransformPoints')))
inverseMatrix2x3Into (data trans) (getField inv 'vM')

t1 = (init (new (at gezira 'ReadFromImageARGB32Flipped')))
setImage t1 (allocateGeziraImage gp)

t2 = (init (new (at gezira 'ReadFromImageARGB32Flipped')))
setImage t2 img

setT1 comp (array inv t1)
setT2 comp t2
setC comp (init (new (at gezira 'CompositeOver')))
setT a comp

w = (init (new (at gezira 'WriteToImageARGB32Flipped')))
setImage w img

openWindow

fill bitmap (color 255 255 255)

gp = (readFrom (new 'PNGReader') (readFile 'tests/gp.png' true))

start = (msecsSinceStart)
for i 360 {
	trans = (beIdentity (init (new Matrix)))
	setRotation trans i
	setTranslation trans 200 200

	t = (init (new (at gezira 'TransformBeziers')))
	setField t 'vM' (data trans)

	c = (init (new (at gezira 'ClipBeziers')))
	setMinx c 0
	setMiny c 0
	setMaxx c 500
	setMaxy c 500

	r = (init (new (at gezira 'Rasterize')))
	a = (init (new (at gezira 'ApplyTexture')))
	comp = (init (new (at gezira 'CompositeTextures')))

	inv = (init (new (at gezira 'TransformPoints')))
	inverseMatrix2x3Into (data trans) (getField inv 'vM')

	t1 = (init (new (at gezira 'ReadFromImageARGB32Flipped')))
	setImage t1 (allocateGeziraImage gp)

	t2 = (init (new (at gezira 'ReadFromImageARGB32Flipped')))
	setImage t2 img

	setT1 comp (array inv t1)
	setT2 comp t2
	setC comp (init (new (at gezira 'CompositeOver')))
	setT a comp

	w = (init (new (at gezira 'WriteToImageARGB32Flipped')))
	setImage w img

	pipe = (geziraPipeline t c r a w)
	geziraFeed pipe (getField box 'data')
	geziraSync

	clearBuffer
	drawBitmap nil bitmap
	flipBuffer
}
msecs = ((msecsSinceStart) - start)
print msecs 'msecs'

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer

geziraShutDown

}

to textureTest {
  bm = (readFrom (new 'PNGReader') (readFile 'tests/gp.png' true))
  txt = (toTexture bm)
  clearBuffer
  flipBuffer
  clearBuffer
  start = (msecsSinceStart)
  for i 360 {
	showTexture nil txt i i 50 1 1 i
//    flipBuffer
  }
  msecs = ((msecsSinceStart) - start)
  flipBuffer
  print msecs 'msecs'
}

to updateTest {
  bm = (readFrom (new 'PNGReader') (readFile 'tests/gp.png' true))
  txt = (toTexture bm)
  start = (msecsSinceStart)
  for i 100 { updateTexture txt bm }
  msecs = ((msecsSinceStart) - start)
  print msecs 'msecs'
}

to spinTest n {
  txt = (toTexture (newBitmap 30 10 (color 200 0 0 255)))
  locations = (list)
  rotations = (list)
  speeds = (list)
  repeat n {
	add locations (array (rand 50 450) (rand 50 450))
	add rotations (rand 360)
	add speeds (rand -10 10)
  }
  frames = 0
  start = (msecsSinceStart)
  while (((msecsSinceStart) - start) < 5000) {
	clearBuffer
	for i (count locations) {
	  p = (at locations i)
	  r = ((at rotations i) + (at speeds i))
	  atPut rotations i r
	  showTexture nil txt (first p) (last p) 255 1 1 r
	}
	flipBuffer
	frames += 1
  }
  msecs = ((msecsSinceStart) - start)
  print frames 'frames in' msecs 'msecs' ((1000.0 * frames) / msecs) 'fps'
}

to spinTestBM n {
  srcBM = (newBitmap 30 10 (color 200 0 0 255))
  locations = (list)
  rotations = (list)
  speeds = (list)
  cache = (list)
  repeat n {
	add locations (array (rand 50 450) (rand 50 450))
	add rotations (rand 360)
	add speeds (rand -10 10)
	add cache nil
  }
  frames = 0
  start = (msecsSinceStart)
  while (((msecsSinceStart) - start) < 5000) {
	clearBuffer
	for i (count locations) {
	  p = (at locations i)
	  r = ((at rotations i) + (at speeds i))
	  atPut rotations i r
	  c = (at cache i)
	  rotatedBM = (scaleAndRotate srcBM 1 1 r c)
	  atPut cache i rotatedBM
	  drawBitmap nil rotatedBM (first p) (last p)
	}
	flipBuffer
	frames += 1
  }
  msecs = ((msecsSinceStart) - start)
  print frames 'frames in' msecs 'msecs' ((1000.0 * frames) / msecs) 'fps'
}
