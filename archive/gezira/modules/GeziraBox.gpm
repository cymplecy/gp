module 'GeziraBox'
  moduleExports Box Fill Shape Slider
  moduleExports linearGradientFill radialGradientFill uniformColorFill
  moduleExports openBoxAsWindow setHandler
  moduleExports Matrix Point Rectangle
  moduleVariables Canvas Matrix Point Rectangle

  to initializeModule canvasMod mathMod {
	if (isNil canvasMod) { canvasMod = (loadModule 'GeziraCanvas') }
	if (isNil mathMod) { mathMod = (loadModule 'GeziraMatrix2x3') }
	setShared 'Canvas' canvasMod
	setShared 'Matrix' (at mathMod 'Matrix')
	setShared 'Point' (at mathMod 'Point')
	setShared 'Rectangle' (at mathMod 'Rectangle')
  }

defineClass Box handler container parts transform shape isPage isWindow
defineClass Shape elements
defineClass BoxShape extent fill stroke path
defineClass Fill fill stroke path

method init Box {
  transform = (beIdentity (new (shared 'Matrix')))
  shape = (init (new 'BoxShape'))

  setExtent shape 50 40
  uni = (init (new (at (shared 'Canvas') 'UniformColorFill')))
  c = (randomColor)
  setAlpha c 180
  setColor uni c
  setFill shape uni

  parts = (list)
  isWindow = false
  isPage = false
  return this
}

method parts Box {
  return parts
}

method outerBounds Box {
  return (transformRect transform (init (new (shared 'Rectangle')) 0 0 (x (extent this)) (y (extent this))))
}

method setPosition Box x y {
  setTranslation transform x y
}

method position Box {
  return (translation transform)
}

method left Box {
  return (left (outerBounds this))
}

method setLeft Box arg1 {
  if ((classOf arg1) == (shared 'Point')) {
    aNumber = (x arg1)
  } else {
    aNumber = arg1
  }
  translateByInContainer this (init (new (shared 'Point')) (aNumber - (left this)) 0)
}

method right Box {
  return (right (outerBounds this))
}

method setRight Box arg1 {
  if ((classOf arg1) == (shared 'Point')) {
    aNumber = (x arg1)
  } else {
    aNumber = arg1
  }
  translateByInContainer this (init (new (shared 'Point')) (aNumber - (right this)) 0)
}

method top Box {
  return (top (outerBounds this))
}

method setTop Box arg1 {
  if ((classOf arg1) == (shared 'Point')) {
    aNumber = (x arg1)
  } else {
    aNumber = arg1
  }
  translateByInContainer this (init (new (shared 'Point')) 0 (aNumber - (top this)))
}

method bottom Box {
  return (bottom (outerBounds this))
}

method setBottom Box arg1 {
  if ((classOf arg1) == (shared 'Point')) {
    aNumber = (x arg1)
  } else {
    aNumber = arg1
  }
  translateByInContainer this (init (new (shared 'Point')) 0 (aNumber - (bottom this)))
}

method center Box {
  return (transformPoint transform ((extent this) * 0.5))
}

method setCenter Box aPoint {
  translateByInContainer this (aPoint - (center this))
}

method translateByInContainer Box aPoint {
  t = (beIdentity (new (shared 'Matrix')))
  setTranslation t aPoint
  m = (composedWith t transform)
  transform = m
}

method keepWithinContainer Box {
  if (isNil container) {return}
  if ((right this) > (width container)) {setRight this (width container)}
  if ((left this) < 0.0) {setLeft this 0.0}
  if ((top this) > (height container)) {setTop this (height container)}
  if ((bottom this) < 0.0) {setBottom this 0.0}
}

method setRotation Box r {
  setRotation transform r
}

method setScale Box x y {
  if (isNil y) {y = x}
  rot = (rotation transform)
  off = (translation transform)

  r = (beIdentity (new (shared 'Matrix')))
  setRotation r rot
  o = (beIdentity (new (shared 'Matrix')))
  setTranslation o off
  s = (beIdentity (new (shared 'Matrix')))
  setScale s x y
  transform = (composedWith o (composedWith r s))
}

method setExtent Box arg1 arg2 {
  if ((classOf arg1) == Point) {
    x = (x arg1)
    y = (y arg1)
  } else {
    x = arg1
    y = arg2
  }
  if (isClass shape 'BoxShape') {
    setExtent shape x y
  }
  // ...
}

method extent Box {
  if (isClass shape 'BoxShape') {
    return (extent shape)
  }
  // ...
}

method width Box {
  if (isClass shape 'BoxShape') {
    return (x (extent shape))
  }
  // ...
}

method height Box {
  if (isClass shape 'BoxShape') {
    return (y (extent shape))
  }
  // ...
}

method containsPoint Box aPoint {
  if (isClass shape 'BoxShape') {
    origin = (init (new (shared 'Point')) 0 0)
    ext = (extent this)
    return (and (origin <= aPoint) (aPoint <= ext))
  }
  return false
}

method setFill Box aFill {
  if (isClass shape 'BoxShape') {
    setFill shape aFill
  }
  // ...
}

method setShape Box aValue {
  shape = aValue
}

method drawOn Box aCanvas {
  oldState = (saveState aCanvas)
  transformBy aCanvas transform

  if (isClass shape 'BoxShape') {
    setFill aCanvas (fill shape)
    setStroke aCanvas (stroke shape)
    drawPath aCanvas (path shape)
    sync aCanvas // better use the gate?
  } else {
    for i (count (elements shape)) {
      elem = (at (elements shape) i)
      setFill aCanvas (fill elem)
      setStroke aCanvas (stroke elem)
      drawPath aCanvas (path elem)
      sync aCanvas // better use the gate?
    }
  }

  for i (count parts) {
    child = (at parts (((count parts) - i) + 1))
    drawOn child aCanvas
  }
  restoreState aCanvas oldState
}

method addPart Box child {
  add parts child
  setContainer child this
}

method setContainer Box aBox {
  container = aBox
}

method invertPoint Box aPoint {
  return (invertPoint transform aPoint)
}

method localPoint Box aPoint {
  if (or isWindow (isNil container)) {
    return aPoint  // (invertPoint transform aPoint)
  }
  return (invertPoint this (localPoint container aPoint))
}

method globalPoint Box aPoint {
  if (or isWindow (isNil container)) {
    return aPoint // (transformPoint transform aPoint)
  }
  return (transformPoint transform (globalPoint container aPoint))
}

method transformFromGlobal Box {
  if (isNil container) {
    return (beIdentity (new (shared 'Matrix')))
  }
  return (composedWith (transformFromGlobal container) transform)
}

method transformToMatchWith Box another {
  tr = (transformFromGlobal another)
  tr2 = (inverse (transformFromGlobal container))
  return (composedWith tr2 tr)
}

method setIsPage Box aBoolean {isPage = aBoolean}
method setIsWindow Box aBoolean {isWindow = aBoolean}
method isWindow Box {return isWindow}

to setHandler aBox aHandler {
  setField aHandler 'box' aBox
  setField aBox 'handler' aHandler
}

defineClass Page box hand canvas bitmap evtTransform
defineClass Hand box page focus

to handMoveOver aHandler { noop }
to handMoveFocus aHandler { noop }
to handEnter aHandler { noop }
to handLeave aHandler { noop }
to handDownOn aHandler { return (dispatchEvent aHandler 'whenDragged') }
to handUpOn aHandler {return false}
to clicked aHandler { return (dispatchEvent aHandler 'whenClicked') }
to touchHold aHandler {return false}
to swipe aHandler scrollX scrollY { return (dispatchEvent aHandler 'whenScrolled' scrollX scrollY) }
to pageResized aHandler { dispatchEvent aHandler 'whenPageResized' }
to aboutToRedrawPage aHandler { noop }
to aboutToBeGrabbed aHandler { noop }
to justReceivedDrop aHandler { noop }
to justGrabbedPart aHandler { noop }
to justDropped aHandler { noop }
to changed aHandler { noop }
to destroyedMorph aHandler { noop}

to wantsDropOf aHandler {return false}
to acceptsEvents aHandler {return false}
to isSelectable aHandler {return false}

to box aHandler { return (getField aHandler 'box') }
to handler aBox { return (getField aBox 'handler') }


method init Page {
  return this
}

method init Hand {
  return this
}

method processEvent Hand evt {
  setPosition box (at evt 'x') (at evt 'y')
  type  = (at evt 'type')
  if (type == 'mouseMove') {
    processMove this (currentObject this)
  } (type == 'mouseDown') {
    processDown this (currentObject this) (at evt 'button')
  } (type == 'mouseUp') {
    processUp this (currentObject this)
  }
}

method objectAt Hand aBox aPoint {
  if (isNil aBox) {
    aBox = (box page)
    aPoint = (position box)
  }
  parts = (parts aBox)
  for i (count parts) {
    part = (at parts (((count parts) - i) + 1))
    result = (objectAt this part (invertPoint part aPoint))
    if (notNil result) {return result}
  }
  if (containsPoint aBox aPoint) {return (handler aBox)}
  return nil
}

method currentObject Hand {
  if (notNil focus) {return focus}
  return (objectAt this)
}

method processMove Hand currentObj {
  if (notNil focus) {handMoveFocus focus this}
}

method processDown Hand currentObj button {
  if (acceptsEvents currentObj) {
    handDownOn currentObj this
  }
}

method processUp Hand {
  if (notNil focus) {
    handUpOn focus this
    focus = nil
  }
}

method setFocus Hand anObject {
  focus = anObject
}

to openBoxAsWindow aBox tryRetina title width height {
  if (isNil tryRetina) {tryRetina = true}
  if (isNil width) {
    if (isNil aBox) {
      width = 500
    } else {
      width = (x (extent aBox))
    }
  }
  if (isNil height) {
    if (isNil aBox) {
      height = 500
    } else {
      height = (x (extent aBox))
    }
  }
  if (isNil aBox) {
    aBox = (init (new 'Box'))
    setExtent aBox width height
    setFill aBox (uniformColorFill (color 250 250 250))
  }

  box = aBox
  page = (init (new 'Page'))
  setHandler box page
  setIsPage box true

  setShared 'window' page (sessionModule)

  box = (init (new 'Box'))
  setExtent box 2 2

  hand = (init (new 'Hand'))
  setHandler box hand
  setFill box (uniformColorFill (color 5 5 5))

  setField hand 'page' page
  setField page 'hand' hand

  ext = (extent box)
  openWindow (max 500 (x ext)) (max 500 (y ext)) tryRetina title

  winSize = (windowSize)
  doubleResolution = ((at winSize 3) == (2 * (at winSize 1)))
  if (or doubleResolution ('iOS' == (platform))) { // retina display
    scale = 2
  } else {
    scale = 1
  }
  evtTransform = (beIdentity (new (shared 'Matrix')))
  setM11 evtTransform scale
  setM12 evtTransform 0
  setM21 evtTransform 0
  setM22 evtTransform (- scale)
  setM31 evtTransform 0
  setM32 evtTransform (height * scale)
  setField page 'evtTransform' evtTransform

  canvas = (init (new (at (shared 'Canvas') 'GeziraCanvas')))
  setField page 'canvas' canvas
  bitmap = (newBitmap (scale * width) (scale * height))
  setField page 'bitmap' bitmap

  setScale (box page) scale

  return page
}

method redraw Page {
  box = (box this)
  ext = (extent box)
  fill bitmap (color 255 255 255)
  setTarget canvas bitmap
  setFlip canvas true
  drawOn box canvas
  drawBitmap nil bitmap
  flipBuffer
}

method startStepping Page {
  while true {step this}
}

method step Page {
  t = (newTimer)
  gcIfNeeded this
  processEvents this
  redraw this
  sleepTime = (15 - (msecs t))
  if (sleepTime > 0) {sleep sleepTime}
}

method gcIfNeeded Page {
  wordsAllocated = (at (memStats) 4)
  if (wordsAllocated > 1000000) { gc }
}

method processEvents Page {
  evt = (getNextEvent)
  if (notNil evt) {translateEvent this evt}
  while (notNil evt) {
    nxt = (getNextEvent)
    type = (at evt 'type')
    if (or (type == 'mouseMove') (type == 'mouseDown') (type == 'mouseUp')) {
      // optimization: out of consecutive mouseMove events only handle the last one
      if (not (and (type == 'mouseMove') (notNil nxt) ((at nxt 'type') == 'mouseMove'))) {
        processEvent hand evt
      }
    }
    evt = nxt
    if (notNil evt) { translateEvent this evt }
  }
}

method translateEvent Page evt {
  x = (at evt 'x')
  y = (at evt 'y')
  if (and (notNil x) (notNil y)) {
    pt = (invertPoint evtTransform (init (new (shared 'Point')) x y))
    atPut evt 'x' (x pt)
    atPut evt 'y' (y pt)
  }
  return evt
}

to getNextEvent {
  // filter out "touch" type events for now
  evt = (nextEvent)
  if (isNil evt) {return nil}
  type = (at evt 'type')
  if (or (type == 'touch') (isClass type 'Integer')) {return (getNextEvent)}
  return evt
}

defineClass Slider box

method init Slider {
  return this
}

method acceptsEvents Slider {
  return true
}

method sliderAction Slider hand {
  setCenter (gripBox this) (localPoint (box this) (position (box hand)))
  keepWithinContainer (gripBox this)
  return true
}

method handDownOn Slider hand {
  setFocus hand this
  return (sliderAction this hand)
}

method handMoveFocus Slider hand {
  return (sliderAction this hand)
}

method handUpOn Slider hand {
  setFocus hand nil
}

method gripBox Slider {
  return (at (parts (box this)) 1)
}

method init Fill aPath {
  path = aPath
  return this
}

method setFill Fill aValue {
  fill = aValue
}

method setStroke Fill aValue {
  stroke = aValue
}

method setPath Fill aValue {
  path = aValue
}

method fill Fill {
  return fill
}

method stroke Fill {
  return stroke
}

method path Fill {
  return path
}

method init Shape {
  elements = (list)
  return this
}

method elements Shape {
  return elements
}

method setElements Shape aValue {
  elements = aValue
}

method init BoxShape {
  extent = (init (new (shared 'Point')))
  return this
}

method setExtent BoxShape x y {
  setX extent x
  setY extent y
}

method extent BoxShape {
  return (init (new (shared 'Point')) (x extent) (y extent))
}

method setFill BoxShape aFill {
  fill = aFill
}

method setStroke BoxShape aStrokeOrNil {
  stroke = aStrokeOrNil
}

method fill BoxShape {
  return fill
}

method stroke BoxShape {
  return stroke
}

method path BoxShape {
  if (isNil path) {
    path = (call (at (shared 'Canvas') 'rectPath') 0 0 (x extent) (y extent))
  }
  return path
}

to linearGradientFill sx sy ex ey {
  fill = (init (new (at (shared 'Canvas') 'LinearGradientFill')))
  setStart fill sx sy
  setEnd fill ex ey
  return fill
}

to radialGradientFill cx cy r {
  fill = (init (new (at (shared 'Canvas') 'RadialGradientFill')))
  setCenter fill cx cy
  setRadius fill r
  return fill
}

to uniformColorFill aColor {
  fill = (init (new (at (shared 'Canvas') 'UniformColorFill')))
  setColor fill aColor
  return fill
}
