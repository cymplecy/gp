to randomUniformFill gezira {
  color = (randomColor)
  setField color 'a' (rand 128 200)
  fill = (init (new (at gezira 'UniformColorFill')))
  setColor fill color
  return fill
}

call (function {
  openWindow

  math = (loadModule 'modules/GeziraMath.gp')
  gezira = (loadModule 'modules/GeziraCanvas.gp' math)
  canvas = (init (new (at gezira 'GeziraCanvas')))
  bitmap = (newBitmap 500 500)
  fill bitmap (color 255 255 255)
  setTarget canvas bitmap

  setStroke canvas (call (at gezira 'stroke'))
  setFill canvas (randomUniformFill gezira)
  drawPath canvas (call (at gezira 'linePath') 50 100 340 130)

  setStroke canvas nil
  setFill canvas (randomUniformFill gezira)
  drawPath canvas (call (at gezira 'rectPath') 70 130 200 100)
  setFill canvas (randomUniformFill gezira)

  setStroke canvas (call (at gezira 'stroke'))
  drawPath canvas (call (at gezira 'arcPath') 0 360 100 350 30 30)

  setStroke canvas nil
  setFill canvas (randomUniformFill gezira)
  drawPath canvas (call (at gezira 'arcPath') 0 360 200 350 60 40)
  drawPath canvas (call (at gezira 'arcPath') 0 360 300 350 60 40)

  sync canvas

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer

  sleep 10000
})

