#!/bin/bash
# Run GP.

currentOS=`uname -s`
if [ "$currentOS" == "Darwin" ]; then
	gp="./mac_gp"
elif [ "$currentOS" == "Linux" ]; then
    gp="./linux_gp"
else
    echo "Unsupported OS"
    exit 1
fi

if [[ `command -v rlwrap` ]]; then
	launcher='rlwrap '$gp
else
	echo "rlwrap not found. Consider installing it to get command history and keyboard"
	echo "navigation in the MicroBlocks REPL."
	launcher=$gp
fi

($launcher runtime/lib/* runtime/startup.gp -)
